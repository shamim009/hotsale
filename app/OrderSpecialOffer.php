<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderSpecialOffer extends Model
{
    protected $fillable = [
        'order_id',	'discount_method',	'offer_type',	'product_id',	'discount_amount',	'shipping_cost'
    ];
}
