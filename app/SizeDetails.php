<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SizeDetails extends Model
{
    protected $table = 'size_details';

    protected $guard = [];
}
