<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialOffer extends Model
{
    protected $fillable = [
        'offer_type','title','product_ids','offer_product_ids','min_buy',	'discount_Price_break','min_qty_Price_break','discount_method','min_qty','discount_qty','discount',	'discount_type'	,'start_date',	'end_date',	'max_discount',	'status'
    ];
}
