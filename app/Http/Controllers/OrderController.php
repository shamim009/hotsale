<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\OTPVerificationController;
use App\Http\Controllers\ClubPointController;
use App\Http\Controllers\AffiliateController;
use App\Order;
use App\Cart;
use App\Address;
use App\Product;
use App\ProductStock;
use App\CommissionHistory;
use App\Color;
use App\OrderDetail;
use App\CouponUsage;
use App\Coupon;
use App\OtpConfiguration;
use App\User;
use App\BusinessSetting;
use App\SpecialOffer;
use App\OrderSpecialOffer;
use Auth;
use Session;
use DB;
use Mail;
use App\Mail\InvoiceEmailManager;
use CoreComponentRepository;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource to seller.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $payment_status = null;
        $delivery_status = null;
        $sort_search = null;
        $orders = DB::table('orders')
                    ->orderBy('code', 'desc')
//                    ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                    ->where('seller_id', Auth::user()->id)
                    ->select('orders.id')
                    ->distinct();

        if ($request->payment_status != null){
            $orders = $orders->where('payment_status', $request->payment_status);
            $payment_status = $request->payment_status;
        }
        if ($request->delivery_status != null) {
            $orders = $orders->where('delivery_status', $request->delivery_status);
            $delivery_status = $request->delivery_status;
        }
        if ($request->has('search')){
            $sort_search = $request->search;
            $orders = $orders->where('code', 'like', '%'.$sort_search.'%');
        }

        $orders = $orders->paginate(15);

        foreach ($orders as $key => $value) {
            $order = \App\Order::find($value->id);
            $order->viewed = 1;
            $order->save();
        }

        return view('frontend.user.seller.orders', compact('orders','payment_status','delivery_status', 'sort_search'));
    }

    // All Orders
    public function all_orders(Request $request)
    {


         $date = $request->date;
         $sort_search = null;
         $orders = Order::orderBy('code', 'desc');
         if ($request->has('search')){
             $sort_search = $request->search;
             $orders = $orders->where('code', 'like', '%'.$sort_search.'%');
         }
         if ($date != null) {
             $orders = $orders->where('created_at', '>=', date('Y-m-d', strtotime(explode(" to ", $date)[0])))->where('created_at', '<=', date('Y-m-d', strtotime(explode(" to ", $date)[1])));
         }
         $orders = $orders->paginate(15);
         return view('backend.sales.all_orders.index', compact('orders', 'sort_search', 'date'));
    }

    public function all_orders_show($id)
    {
         $order = Order::findOrFail(decrypt($id));
         $order_shipping_address = json_decode($order->shipping_address);
         $delivery_boys = User::where('city', $order_shipping_address->city)
                ->where('user_type', 'delivery_boy')
                ->get();

         return view('backend.sales.all_orders.show', compact('order', 'delivery_boys'));
    }

    // Inhouse Orders
    public function admin_orders(Request $request)
    {


        $date = $request->date;
        $payment_status = null;
        $delivery_status = null;
        $sort_search = null;
        $admin_user_id = User::where('user_type', 'admin')->first()->id;
        $orders = DB::table('orders')
                    ->orderBy('code', 'desc')
//                    ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                    ->where('seller_id', $admin_user_id)
                    ->select('orders.id')
                    ->distinct();

        if ($request->payment_type != null){
            $orders = $orders->where('payment_status', $request->payment_type);
            $payment_status = $request->payment_type;
        }
        if ($request->delivery_status != null) {
            $orders = $orders->where('delivery_status', $request->delivery_status);
            $delivery_status = $request->delivery_status;
        }
        if ($request->has('search')){
            $sort_search = $request->search;
            $orders = $orders->where('code', 'like', '%'.$sort_search.'%');
        }
        if ($date != null) {
            $orders = $orders->whereDate('orders.created_at', '>=', date('Y-m-d', strtotime(explode(" to ", $date)[0])))->whereDate('orders.created_at', '<=', date('Y-m-d', strtotime(explode(" to ", $date)[1])));
        }

        $orders = $orders->paginate(15);
        return view('backend.sales.inhouse_orders.index', compact('orders','payment_status','delivery_status', 'sort_search', 'admin_user_id', 'date'));
    }

    public function show($id)
    {
        $order = Order::findOrFail(decrypt($id));
        $order->viewed = 1;
        $order->save();
        return view('backend.sales.inhouse_orders.show', compact('order'));
    }

    // Seller Orders
    public function seller_orders(Request $request)
    {


        $date = $request->date;
        $seller_id = $request->seller_id;
        $payment_status = null;
        $delivery_status = null;
        $sort_search = null;
        $admin_user_id = User::where('user_type', 'admin')->first()->id;
        $orders = DB::table('orders')
                    ->orderBy('code', 'desc')
//                    ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                    ->where('orders.seller_id', '!=' ,$admin_user_id)
                    ->select('orders.id')
                    ->distinct();

        if ($request->payment_type != null){
            $orders = $orders->where('orders.payment_status', $request->payment_type);
            $payment_status = $request->payment_type;
        }
        if ($request->delivery_status != null) {
            $orders = $orders->where('delivery_status', $request->delivery_status);
            $delivery_status = $request->delivery_status;
        }
        if ($request->has('search')){
            $sort_search = $request->search;
            $orders = $orders->where('code', 'like', '%'.$sort_search.'%');
        }
        if ($date != null) {
            $orders = $orders->whereDate('orders.created_at', '>=', date('Y-m-d', strtotime(explode(" to ", $date)[0])))->whereDate('orders.created_at', '<=', date('Y-m-d', strtotime(explode(" to ", $date)[1])));
        }
        if ($seller_id) {
            $orders = $orders->where('seller_id', $seller_id);
        }

        $orders = $orders->paginate(15);
        return view('backend.sales.seller_orders.index', compact('orders', 'payment_status', 'delivery_status', 'sort_search', 'admin_user_id', 'seller_id', 'date'));
    }

    public function seller_orders_show($id)
    {
        $order = Order::findOrFail(decrypt($id));
        $order->viewed = 1;
        $order->save();
        return view('backend.sales.seller_orders.show', compact('order'));
    }


    // Pickup point orders
    public function pickup_point_order_index(Request $request)
    {
        $date = $request->date;
        $sort_search = null;

        if (Auth::user()->user_type == 'staff' && Auth::user()->staff->pick_up_point != null) {
            //$orders = Order::where('pickup_point_id', Auth::user()->staff->pick_up_point->id)->get();
            $orders = DB::table('orders')
                        ->orderBy('code', 'desc')
                        ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                        ->where('order_details.pickup_point_id', Auth::user()->staff->pick_up_point->id)
                        ->select('orders.id')
                        ->distinct();

            if ($request->has('search')){
                $sort_search = $request->search;
                $orders = $orders->where('code', 'like', '%'.$sort_search.'%');
            }
            if ($date != null) {
                $orders = $orders->whereDate('orders.created_at', '>=', date('Y-m-d', strtotime(explode(" to ", $date)[0])))->whereDate('orders.created_at', '<=', date('Y-m-d', strtotime(explode(" to ", $date)[1])));
            }

            $orders = $orders->paginate(15);

            return view('backend.sales.pickup_point_orders.index', compact('orders', 'sort_search', 'date'));
        }
        else{
            //$orders = Order::where('shipping_type', 'Pick-up Point')->get();
            $orders = DB::table('orders')
                        ->orderBy('code', 'desc')
                        ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                        ->where('order_details.shipping_type', 'pickup_point')
                        ->select('orders.id')
                        ->distinct();

            if ($request->has('search')){
                $sort_search = $request->search;
                $orders = $orders->where('code', 'like', '%'.$sort_search.'%');
            }
            if ($date != null) {
                $orders = $orders->whereDate('orders.created_at', '>=', date('Y-m-d', strtotime(explode(" to ", $date)[0])))->whereDate('orders.created_at', '<=', date('Y-m-d', strtotime(explode(" to ", $date)[1])));
            }

            $orders = $orders->paginate(15);

            return view('backend.sales.pickup_point_orders.index', compact('orders', 'sort_search', 'date'));
        }
    }

    public function pickup_point_order_sales_show($id)
    {
        if (Auth::user()->user_type == 'staff') {
            $order = Order::findOrFail(decrypt($id));
            return view('backend.sales.pickup_point_orders.show', compact('order'));
        }
        else{
            $order = Order::findOrFail(decrypt($id));
            return view('backend.sales.pickup_point_orders.show', compact('order'));
        }
    }

    /**
     * Display a single sale to admin.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());


        $order = new Order;
        if(Auth::check()){
            $order->user_id = Auth::user()->id;
        }
        else{
            $order->guest_id = mt_rand(100000, 999999);
        }

        $carts = Cart::where('user_id', Auth::user()->id)
                ->where('owner_id', $request->owner_id)
                ->get();

        $shipping_info = Address::where('id', $carts[0]['address_id'])->first();
        $shipping_info->name = Auth::user()->name;
        $shipping_info->email = Auth::user()->email;
        $order->seller_id = $request->owner_id;
        $order->shipping_address = json_encode($shipping_info);

        $order->payment_type = $request->payment_option;
        $order->delivery_viewed = '0';
        $order->payment_status_viewed = '0';
        $order->code = date('Ymd-His').rand(10,99);
        $order->date = strtotime('now');

        if($order->save()){
            $subtotal = 0;
            $tax = 0;
            $shipping = 0;
            $total_quantity  = 0;

            //calculate shipping is to get shipping costs of different types
            $admin_products = array();
            $seller_products = array();

            //Order Details Storing
            foreach ($carts as $key => $cartItem){
                $product = Product::find($cartItem['product_id']);

                if($product->added_by == 'admin') {
                    array_push($admin_products, $cartItem['product_id']);
                }
                else {
                    $product_ids = array();
                    if(array_key_exists($product->user_id, $seller_products)){
                        $product_ids = $seller_products[$product->user_id];
                    }
                    array_push($product_ids, $cartItem['product_id']);
                    $seller_products[$product->user_id] = $product_ids;
                }

                $subtotal += $cartItem['price']*$cartItem['quantity'];
                $tax += $cartItem['tax']*$cartItem['quantity'];

                $total_quantity += $cartItem['quantity'];

                $product_variation = $cartItem['variation'];

                $product_stock = $product->stocks->where('variant', $product_variation)->first();
                if($product->digital != 1 &&  $cartItem['quantity'] > $product_stock->qty) {
                    flash(translate('The requested quantity is not available for ').$product->getTranslation('name'))->warning();
                    $order->delete();
                    return redirect()->route('cart')->send();
                }
                else {
                    $product_stock->qty -= $cartItem['quantity'];
                    $product_stock->save();
                }

                $order_detail = new OrderDetail;
                $order_detail->order_id  = $order->id;
                $order_detail->seller_id = $product->user_id;
                $order_detail->product_id = $product->id;
                $order_detail->variation = $product_variation;
                $order_detail->price = $cartItem['price'] * $cartItem['quantity'];
                $order_detail->tax = $cartItem['tax'] * $cartItem['quantity'];
                $order_detail->shipping_type = $cartItem['shipping_type'];
                $order_detail->product_referral_code = $cartItem['product_referral_code'];

                $order_detail->shipping_cost = $cartItem['shipping_cost'];

                if($product->is_quantity_multiplied == 1 && get_setting('shipping_type') == 'product_wise_shipping') {
                    $order_detail->shipping_cost = $order_detail->shipping_cost * $cartItem['quantity'];
                }
                $shipping += $order_detail->shipping_cost;

                if ($cartItem['shipping_type'] == 'pickup_point') {
                    $order_detail->pickup_point_id = $cartItem['pickup_point'];
                }
                //End of storing shipping cost

                $order_detail->quantity = $cartItem['quantity'];
                $order_detail->save();

                $product->num_of_sale++;
                $product->save();

                if (\App\Addon::where('unique_identifier', 'affiliate_system')->first() != null &&
                        \App\Addon::where('unique_identifier', 'affiliate_system')->first()->activated) {
                    if($order_detail->product_referral_code) {
                        $referred_by_user = User::where('referral_code', $order_detail->product_referral_code)->first();

                        $affiliateController = new AffiliateController;
                        $affiliateController->processAffiliateStats($referred_by_user->id, 0, $order_detail->quantity, 0, 0);
                    }
                }
            }



            //For special offer
            $special_offer = SpecialOffer::where('status', 1)->latest()->first();
            $date = strtotime(date('d-m-Y'));

            if($special_offer != null && $date >= $special_offer->start_date && $date <= $special_offer->end_date){


                if ($special_offer->offer_type == 'cart_base') {
                    if($special_offer->discount_method == 'delivery_chare_on_amount'){

                        if($subtotal >= $special_offer->min_buy){

                            $order_special = new OrderSpecialOffer;
                            $order_special->offer_type  = 'cart_base';
                            $order->offer_id = $special_offer->id;
                            $order_special->order_id  = $order->id;
                            $order_special->discount_method  = 'delivery_chare_on_amount';
                            $order_special->shipping_cost  = 0;
                            $order_special->save();
                        }

                    }else if($special_offer->discount_method == 'delivery_chare_on_quantity'){
                        if($total_quantity >= $special_offer->min_qty){

                            $order_special = new OrderSpecialOffer;
                            $order_special->offer_type  = 'cart_base';
                            $order->offer_id = $special_offer->id;
                            $order_special->order_id  = $order->id;
                            $order_special->discount_method  = 'delivery_chare_on_amount';
                            $order_special->shipping_cost  = 0;
                            $order_special->save();

                        }
                    }else if($special_offer->discount_method == 'flat_discount_offer'){
                        if($subtotal >= $special_offer->min_buy){

                            if($special_offer->discount_type == 'amount'){
                                $discount = $special_offer->discount;
                            }else if($special_offer->discount_type == 'percent'){
                                $discount = ($subtotal * $special_offer->discount) / 100;
                                if ($discount > $special_offer->max_discount) {
                                    $discount = $special_offer->max_discount;
                                }
                            }

                            $order_special = new OrderSpecialOffer;
                            $order_special->offer_type  = 'cart_base';
                            $order->offer_id = $special_offer->id;
                            $order_special->order_id  = $order->id;
                            $order_special->discount_method  = 'flat_discount_offer';
                            $order_special->discount_amount	  = $discount;
                            $order_special->save();

                        }
                    }else if($special_offer->discount_method == 'discount_on_quantiry'){
                        if($total_quantity >= $special_offer->min_qty){
                            $total = 0;

                            foreach ($carts as $key => $cartItem){
                                $total = $total + $cartItem['price']*$cartItem['quantity'];
                                if($total_quantity >= $special_offer->min_qty){
                                    if($special_offer->discount_type == 'amount') {
                                        $discount = $special_offer->discount;
                                    }else if ($special_offer->discount_type == 'percent') {
                                        $discount = ($total * $special_offer->discount) / 100;
                                        if ($discount > $special_offer->max_discount) {
                                            $discount = $special_offer->max_discount;
                                        }
                                    }

                                }
                            }

                            $order_special = new OrderSpecialOffer;
                            $order_special->offer_type  = 'cart_base';
                            $order->offer_id = $special_offer->id;
                            $order_special->order_id  = $order->id;
                            $order_special->discount_method  = 'discount_on_quantiry';
                            $order_special->discount_amount	  = $discount;
                            $order_special->save();

                        }

                    }else if($special_offer->discount_method == 'quantity_offer'){

                        if($total_quantity >= $special_offer->min_qty){
                            $discount = 0;
                            foreach ($carts as $key => $cartItem){
                                $total_quantity += $cartItem['quantity'];
                                if($total_quantity >= $special_offer->min_qty){
                                    $discount = $cartItem['price']*$special_offer->discount_qty;
                                }
                            }

                            $order_special = new OrderSpecialOffer;
                            $order_special->offer_type  = 'cart_base';
                            $order->offer_id = $special_offer->id;
                            $order_special->order_id  = $order->id;
                            $order_special->discount_method  = 'quantity_offer';
                            $order_special->discount_amount	  = $discount;
                            $order_special->save();
                        }

                    }
                }else if($special_offer->offer_type == 'product_base'){
                    if($special_offer->discount_method == 'price_break_discount'){

                        $offered_product_id = json_decode($special_offer->product_ids);
                        $offered_product_qty = json_decode($special_offer->min_qty_Price_break);
                        $offered_product_price = json_decode($special_offer->discount_Price_break);

                        $quantity_count = count($offered_product_price);
                        $discount = 0;$total = 0;
                        $offer_qty_exist = 0;$discount_offer = 0;

                        foreach ($carts as $key => $cartItem){
                            $item_quantity = $cartItem['quantity'];
                            $product_id = $cartItem['product_id'];
                            $total = $total + $cartItem['price']*$cartItem['quantity'];

                            if(in_array($product_id,$offered_product_id)){
                                $offer_qty_exist += $cartItem['quantity'];
                            }
                        }
                        foreach($offered_product_qty as $key=>$offered_qty){
                            if($offered_qty == $offer_qty_exist){
                                $discount_offer = $offered_product_price[$key];
                            }else if($offer_qty_exist > $offered_product_qty[$key]){
                                $discount_offer = 0;
                                $discount_offer = $offered_product_price[$key];
                            }
                        }

                        $order_special = new OrderSpecialOffer;
                        $order_special->offer_type  = 'product_base';
                        $order->offer_id = $special_offer->id;
                        $order_special->order_id  = $order->id;
                        $order_special->discount_method  = 'price_break_discount';
                        $order_special->discount_amount	  = $discount_offer;
                        $order_special->save();

                    }else if($special_offer->discount_method == 'buy_get_offer'){
                        foreach ($carts as $key => $cartItem){
                            $product_id_belong_offer = json_decode($special_offer->product_ids);
                            $product_id_with_offer = $special_offer->offer_product_ids;
                            $product_id = $cartItem['product_id'];

                            if(in_array($product_id,$product_id_belong_offer)){
                                $product = \App\Product::find($product_id_with_offer);
                                $cartItem['price'] = 0;
                                $cartItem['quantity'] = 1;

                                $order_detail = new OrderDetail;
                                $order_detail->order_id  = $order->id;
                                $order_detail->seller_id = $product->user_id;
                                $order_detail->product_id = $product->id;
                                $order_detail->price = $cartItem['price'];
                                $order_detail->shipping_type = 'home_delivery';
                                $order_detail->quantity = $cartItem['quantity'];
                                $order_detail->save();

                                $order_special = new OrderSpecialOffer;
                                $order_special->offer_type  = 'product_base';
                                $order->offer_id = $special_offer->id;
                                $order_special->order_id  = $order->id;
                                $order_special->discount_method  = 'buy_get_offer';
                                $order_detail->product_id = $product->id;
                                $order_special->save();
                            }
                        }


                    }else if($special_offer->discount_method == 'flat_discount_offer'){
                        $total = 0;
                        $offer_amount = 0;$discount = 0;
                        $offered_product_id = json_decode($special_offer->product_ids);

                        foreach ($carts as $key => $cartItem){
                            $product_id = $cartItem['product_id'];
                            $total = $total + $cartItem['price']*$cartItem['quantity'];

                            if(in_array($product_id,$offered_product_id)){
                                $offer_amount += $cartItem['price']*$cartItem['quantity'];
                            }
                        }
                        if($offer_amount >= $special_offer->min_buy && $date >= $special_offer->start_date && $date <= $special_offer->end_date){
                            if($special_offer->discount_type == 'amount') {
                                $total -= $special_offer->discount;
                            }else if ($special_offer->discount_type == 'percent') {
                                $discount = ($offer_amount * $special_offer->discount) / 100;
                                if ($discount > $special_offer->max_discount) {
                                    $discount = $special_offer->max_discount;
                                }
                            }
                        }

                        $order_special = new OrderSpecialOffer;
                        $order_special->offer_type  = 'product_base';
                        $order->offer_id = $special_offer->id;
                        $order_special->order_id  = $order->id;
                        $order_special->discount_method  = 'flat_discount_offer';
                        $order_special->discount_amount	  = $discount;
                        $order_special->save();

                    }else if($special_offer->discount_method == 'discount_on_quantiry'){
                        $total = 0;
                        $offer_amount = 0;$discount = 0;$offer_quantity=0;
                        $offered_product_id = json_decode($special_offer->product_ids);

                        foreach ($carts as $key => $cartItem){
                            $product_id = $cartItem['product_id'];
                            $total = $total + $cartItem['price']*$cartItem['quantity'];

                            if(in_array($product_id,$offered_product_id)){
                                $offer_amount += $cartItem['price']*$cartItem['quantity'];
                                $offer_quantity += $cartItem['quantity'];
                            }
                        }
                        if($offer_quantity >= $special_offer->min_qty && $date >= $special_offer->start_date && $date <= $special_offer->end_date){
                            if($special_offer->discount_type == 'amount') {
                                $total -= $special_offer->discount;
                            }else if ($special_offer->discount_type == 'percent') {
                                $discount = ($offer_amount * $special_offer->discount) / 100;
                                if ($discount > $special_offer->max_discount) {
                                    $discount = $special_offer->max_discount;
                                }
                            }
                        }
                        $order_special = new OrderSpecialOffer;
                        $order_special->offer_type  = 'product_base';
                        $order->offer_id = $special_offer->id;
                        $order_special->order_id  = $order->id;
                        $order_special->discount_method  = 'discount_on_quantiry';
                        $order_special->discount_amount	  = $discount;
                        $order_special->save();

                    }else if($special_offer->discount_method == 'quantity_offer'){
                        $total = 0;
                        $discount = 0;$offer_quantity=0;
                        $offered_product_id = json_decode($special_offer->product_ids);

                        foreach ($carts as $key => $cartItem){
                            $product_id = $cartItem['product_id'];
                            $total = $total + $cartItem['price']*$cartItem['quantity'];
                            if(in_array($product_id,$offered_product_id)){
                                $offer_quantity += $cartItem['quantity'];
                            }
                        }
                        if($offer_quantity >= $special_offer->min_qty){
                            $discount = $cartItem['price']*$special_offer->discount_qty;
                        }

                        $order_special = new OrderSpecialOffer;
                        $order_special->offer_type  = 'product_base';
                        $order->offer_id = $special_offer->id;
                        $order_special->order_id  = $order->id;
                        $order_special->discount_method  = 'quantity_offer';
                        $order_special->discount_amount	  = $discount;
                        $order_special->save();

                    }
                }else {
                    $order->offer_id = '';
                }

            }

            //For special offer


            $order->grand_total = $subtotal + $tax + $shipping;

            if ($carts->first()->coupon_code != '') {
//            if(Session::has('club_point')){
                $order->grand_total -= $carts->sum('discount');
                // $order->club_point = Session::get('club_point');
                $order->coupon_discount = $carts->sum('discount');

                // $clubpointController = new ClubPointController;
                // $clubpointController->deductClubPoints($order->user_id, Session::get('club_point'));

                $coupon_usage = new CouponUsage;
                $coupon_usage->user_id = Auth::user()->id;
                $coupon_usage->coupon_id = Coupon::where('code', $carts->first()->coupon_code)->first()->id;
                $coupon_usage->save();
            }

            $order->save();

            $array['view'] = 'emails.invoice';
            $array['subject'] = translate('Your order has been placed').' - '.$order->code;
            $array['from'] = env('MAIL_FROM_ADDRESS');
            $array['order'] = $order;

            foreach($seller_products as $key => $seller_product){
                try {
                    Mail::to(\App\User::find($key)->email)->queue(new InvoiceEmailManager($array));
                } catch (\Exception $e) {

                }
            }

            if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated && \App\OtpConfiguration::where('type', 'otp_for_order')->first()->value){
                try {
                    $otpController = new OTPVerificationController;
                    $otpController->send_order_code($order);
                } catch (\Exception $e) {

                }
            }

            //sends email to customer with the invoice pdf attached
            if(env('MAIL_USERNAME') != null){
                try {
                    Mail::to(Auth::user()->email)->queue(new InvoiceEmailManager($array));
                    Mail::to(User::where('user_type', 'admin')->first()->email)->queue(new InvoiceEmailManager($array));
                } catch (\Exception $e) {

                }
            }

            $request->session()->put('order_id', $order->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::findOrFail($id);
        if($order != null){
            foreach($order->orderDetails as $key => $orderDetail){
                try {

                    $product_stock = ProductStock::where('product_id', $orderDetail->product_id)->where('variant', $orderDetail->variation)->first();
                    if($product_stock != null){
                        $product_stock->qty += $orderDetail->quantity;
                        $product_stock->save();
                    }

                } catch (\Exception $e) {

                }

                $orderDetail->delete();
            }
            $order->delete();
            flash(translate('Order has been deleted successfully'))->success();
        }
        else{
            flash(translate('Something went wrong'))->error();
        }
        return back();
    }

    public function order_details(Request $request)
    {
        $order = Order::findOrFail($request->order_id);
        $order->save();
        return view('frontend.user.seller.order_details_seller', compact('order'));
    }

    public function update_delivery_status(Request $request)
    {
        $order = Order::findOrFail($request->order_id);
        $order->delivery_viewed = '0';
        $order->delivery_status = $request->status;
        $order->save();

        if(Auth::user()->user_type == 'seller') {
            foreach($order->orderDetails->where('seller_id', Auth::user()->id) as $key => $orderDetail){
                $orderDetail->delivery_status = $request->status;
                $orderDetail->save();

                if($request->status == 'cancelled') {
                    $product_stock = ProductStock::where('product_id', $orderDetail->product_id)
                            ->where('variant', $orderDetail->variation)
                            ->first();
                    if($product_stock != null){
                        $product_stock->qty += $orderDetail->quantity;
                        $product_stock->save();
                    }
                }
            }
        }
        else {
            foreach ($order->orderDetails as $key => $orderDetail) {

                $orderDetail->delivery_status = $request->status;
                $orderDetail->save();

                if ($request->status == 'cancelled') {
//
                    $product_stock = ProductStock::where('product_id', $orderDetail->product_id)
                            ->where('variant', $orderDetail->variation)
                            ->first();

                    if ($product_stock != null) {
                        $product_stock->qty += $orderDetail->quantity;
                        $product_stock->save();
                    }
                }

                if (\App\Addon::where('unique_identifier', 'affiliate_system')->first() != null && \App\Addon::where('unique_identifier', 'affiliate_system')->first()->activated) {
                    if (($request->status == 'delivered' || $request->status == 'cancelled') &&
                            $orderDetail->product_referral_code) {

                        $no_of_delivered = 0;
                        $no_of_canceled = 0;

                        if($request->status == 'delivered') {
                            $no_of_delivered = $orderDetail->quantity;
                        }
                        if($request->status == 'cancelled') {
                            $no_of_canceled = $orderDetail->quantity;
                        }

                        $referred_by_user = User::where('referral_code', $orderDetail->product_referral_code)->first();

                        $affiliateController = new AffiliateController;
                        $affiliateController->processAffiliateStats($referred_by_user->id, 0, 0, $no_of_delivered, $no_of_canceled);
                    }
                }
            }
        }

        if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated && \App\OtpConfiguration::where('type', 'otp_for_delivery_status')->first()->value){
            try {
                $otpController = new OTPVerificationController;
                $otpController->send_delivery_status($order);
            } catch (\Exception $e) {
            }
        }

        if (\App\Addon::where('unique_identifier', 'delivery_boy')->first() != null &&
                \App\Addon::where('unique_identifier', 'delivery_boy')->first()->activated) {

            if(Auth::user()->user_type == 'delivery_boy') {
                $deliveryBoyController = new DeliveryBoyController;
                $deliveryBoyController->store_delivery_history($order);
            }
        }

        return 1;
    }

    public function update_payment_status(Request $request)
    {
        $order = Order::findOrFail($request->order_id);
        $order->payment_status_viewed = '0';
        $order->save();

        if(Auth::user()->user_type == 'seller'){
            foreach($order->orderDetails->where('seller_id', Auth::user()->id) as $key => $orderDetail){
                $orderDetail->payment_status = $request->status;
                $orderDetail->save();
            }
        }
        else{
            foreach($order->orderDetails as $key => $orderDetail){
                $orderDetail->payment_status = $request->status;
                $orderDetail->save();
            }
        }

        $status = 'paid';
        foreach($order->orderDetails as $key => $orderDetail){
            if($orderDetail->payment_status != 'paid'){
                $status = 'unpaid';
            }
        }
        $order->payment_status = $status;
        $order->save();


        if($order->payment_status == 'paid' && $order->commission_calculated == 0){
            commission_calculation($order);

            $order->commission_calculated = 1;
            $order->save();
        }

        if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated && \App\OtpConfiguration::where('type', 'otp_for_paid_status')->first()->value){
            try {
                $otpController = new OTPVerificationController;
                $otpController->send_payment_status($order);
            } catch (\Exception $e) {
            }
        }
        return 1;
    }

    public function assign_delivery_boy(Request $request) {
        if (\App\Addon::where('unique_identifier', 'delivery_boy')->first() != null &&
                \App\Addon::where('unique_identifier', 'delivery_boy')->first()->activated) {

            $order = Order::findOrFail($request->order_id);
            $order->assign_delivery_boy = $request->delivery_boy;
            $order->delivery_history_date = date("Y-m-d H:i:s");
            $order->save();

            $delivery_history = \App\DeliveryHistory::where('order_id', $order->id)
                    ->where('delivery_status', $order->delivery_status)
                    ->first();

            if(empty($delivery_history)){
                $delivery_history = new \App\DeliveryHistory;

                $delivery_history->order_id         = $order->id;
                $delivery_history->delivery_status  = $order->delivery_status;
                $delivery_history->payment_type     = $order->payment_type;
            }
            $delivery_history->delivery_boy_id  = $request->delivery_boy;

            $delivery_history->save();

            if(env('MAIL_USERNAME') != null && get_setting('delivery_boy_mail_notification') == '1') {
                $array['view'] = 'emails.invoice';
                $array['subject'] = translate('You are assigned to delivery an order. Order code').' - '.$order->code;
                $array['from'] = env('MAIL_FROM_ADDRESS');
                $array['order'] = $order;

                try {
                    Mail::to($order->delivery_boy->email)->queue(new InvoiceEmailManager($array));
                } catch (\Exception $e) {

                }
            }

            if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null &&
                    \App\Addon::where('unique_identifier', 'otp_system')->first()->activated &&
                    get_setting('delivery_boy_otp_notification') == '1') {
                try {
                    sendSMS($order->delivery_boy->phone,
                            env('APP_NAME'),
                            'You are assigned to delivery an order. Order code : '.$order->code);
                } catch (\Exception $e) {

                }
            }
        }

        return 1;
    }
}
