<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductTranslation;
use App\ProductStock;
use App\Category;
use App\FlashDealProduct;
use App\ProductTax;
use App\Language;
use Auth;
use App\SubSubCategory;
use Session;
use Carbon\Carbon;
use ImageOptimizer;
use DB;
use Combinations;
use CoreComponentRepository;
use Illuminate\Support\Str;
use Artisan;
use App\SizeDetails;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_products(Request $request)
    {
        //CoreComponentRepository::instantiateShopRepository();

        $type = 'In House';
        $col_name = null;
        $query = null;
        $sort_search = null;

        $products = Product::where('added_by', 'admin');

        if ($request->type != null){
            $var = explode(",", $request->type);
            $col_name = $var[0];
            $query = $var[1];
            $products = $products->orderBy($col_name, $query);
            $sort_type = $request->type;
        }
        if ($request->search != null){
            $products = $products
                        ->where('name', 'like', '%'.$request->search.'%');
            $sort_search = $request->search;
        }

        $products = $products->where('digital', 0)->orderBy('created_at', 'desc')->paginate(15);

        return view('backend.product.products.index', compact('products','type', 'col_name', 'query', 'sort_search'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function seller_products(Request $request)
    {
        $col_name = null;
        $query = null;
        $seller_id = null;
        $sort_search = null;
        $products = Product::where('added_by', 'seller');
        if ($request->has('user_id') && $request->user_id != null) {
            $products = $products->where('user_id', $request->user_id);
            $seller_id = $request->user_id;
        }
        if ($request->search != null){
            $products = $products
                        ->where('name', 'like', '%'.$request->search.'%');
            $sort_search = $request->search;
        }
        if ($request->type != null){
            $var = explode(",", $request->type);
            $col_name = $var[0];
            $query = $var[1];
            $products = $products->orderBy($col_name, $query);
            $sort_type = $request->type;
        }

        $products = $products->where('digital', 0)->orderBy('created_at', 'desc')->paginate(15);
        $type = 'Seller';

        return view('backend.product.products.index', compact('products','type', 'col_name', 'query', 'seller_id', 'sort_search'));
    }

    public function all_products(Request $request)
    {
        $col_name = null;
        $query = null;
        $seller_id = null;
        $sort_search = null;
        $products = Product::orderBy('created_at', 'desc');
        if ($request->has('user_id') && $request->user_id != null) {
            $products = $products->where('user_id', $request->user_id);
            $seller_id = $request->user_id;
        }
        if ($request->search != null){
            $products = $products
                        ->where('name', 'like', '%'.$request->search.'%');
            $sort_search = $request->search;
        }
        if ($request->type != null){
            $var = explode(",", $request->type);
            $col_name = $var[0];
            $query = $var[1];
            $products = $products->orderBy($col_name, $query);
            $sort_type = $request->type;
        }

        $products = $products->paginate(15);
        $type = 'All';

        return view('backend.product.products.index', compact('products','type', 'col_name', 'query', 'seller_id', 'sort_search'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('parent_id', 0)
            ->where('digital', 0)
            ->with('childrenCategories')
            ->get();

        return view('backend.product.products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $refund_request_addon = \App\Addon::where('unique_identifier', 'refund_request')->first();

        $product = new Product;
        $product->name = $request->name;
        $product->added_by = $request->added_by;
        if(Auth::user()->user_type == 'seller'){
            $product->user_id = Auth::user()->id;
        }
        else{
            $product->user_id = \App\User::where('user_type', 'admin')->first()->id;
        }
        $product->category_id = $request->category_id;
        //$product->brand_id = $request->brand_id;
        //$product->current_stock = $request->current_stock;
        $product->barcode = $request->barcode;

        if ($refund_request_addon != null && $refund_request_addon->activated == 1) {
            if ($request->refundable != null) {
                $product->refundable = 1;
            }
            else {
                $product->refundable = 0;
            }
        }
        $product->photos = $request->photos;
        $product->measurement_image = $request->measurement_image;
        $product->thumbnail_img = $request->thumbnail_img;
        $product->unit = $request->unit;
        $product->min_qty = $request->min_qty;
        $product->low_stock_quantity = $request->low_stock_quantity;
        $product->priority = $request->priority;
        $product->stock_visibility_state = $request->stock_visibility_state;

        $tags = array();
        if($request->tags[0] != null){
            foreach (json_decode($request->tags[0]) as $key => $tag) {
                array_push($tags, $tag->value);
            }
        }
        $product->tags = implode(',', $tags);

        $product->description = $request->description;
        $product->video_provider = $request->video_provider;
        $product->video_link = $request->video_link;
        $product->unit_price = $request->unit_price;
        //$product->purchase_price = $request->purchase_price;
        //$product->tax = $request->tax;
        //$product->tax_type = $request->tax_type;
        $product->discount = $request->discount;
        $product->discount_type = $request->discount_type;
        $product->shipping_type = $request->shipping_type;
        $product->est_shipping_days  = $request->est_shipping_days;

        if ($request->has('shipping_type')) {
            if($request->shipping_type == 'free'){
                $product->shipping_cost = 0;
            }
            elseif ($request->shipping_type == 'flat_rate') {
                $product->shipping_cost = $request->flat_shipping_cost;
            }
            elseif ($request->shipping_type == 'product_wise') {
                $product->shipping_cost = json_encode($request->shipping_cost);
            }
        }
        if ($request->has('is_quantity_multiplied')) {
            $product->is_quantity_multiplied = 1;
        }

        $product->meta_title = $request->meta_title;
        $product->meta_description = $request->meta_description;

        if($request->has('meta_img')){
            $product->meta_img = $request->meta_img;
        } else {
            $product->meta_img = $product->thumbnail_img;
        }

        if($product->meta_title == null) {
            $product->meta_title = $product->name;
        }

        if($product->meta_description == null) {
            $product->meta_description = strip_tags($product->description);
        }

        if($product->meta_img == null) {
            $product->meta_img = $product->thumbnail_img;
        }

        if($request->hasFile('pdf')){
            $product->pdf = $request->pdf->store('uploads/products/pdf');
        }

        $product->slug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->name)).'-'.Str::random(5);

        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $product->colors = json_encode($request->colors);
        }
        else {
            $colors = array();
            $product->colors = json_encode($colors);
        }

        $choice_options = array();

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $str = 'choice_options_'.$no;

                $item['attribute_id'] = $no;

                $data = array();
                foreach (json_decode($request[$str][0]) as $key => $eachValue) {
                    array_push($data, $eachValue->value);
                }

                $item['values'] = $data;
                array_push($choice_options, $item);
            }
        }

        if (!empty($request->choice_no)) {
            $product->attributes = json_encode($request->choice_no);
        }
        else {
            $product->attributes = json_encode(array());
        }

        $product->choice_options = json_encode($choice_options, JSON_UNESCAPED_UNICODE);

        $product->published = 1;
        if($request->button == 'unpublish' || $request->button == 'draft') {
            $product->published = 0;
        }

        if ($request->has('cash_on_delivery')) {
            $product->cash_on_delivery = 1;
        }
        if ($request->has('featured')) {
            $product->featured = 1;
        }
        if ($request->has('todays_deal')) {
            $product->todays_deal = 1;
        }
        $product->cash_on_delivery = 0;
        if ($request->cash_on_delivery) {
            $product->cash_on_delivery = 1;
        }
        //$variations = array();

        $product->save();

        //Size details
        if($request->has('comboPack')){
            $sizeName = '';
            if(count($request->shirtSize) > count($request->pantSize)){
                $sizeName = 'shirtSize';
            }else{
                $sizeName = 'pantSize';
            }
            foreach($request->$sizeName as $key => $value){
                $query = new SizeDetails();
                $query->product_id = $product->id;
                $query->comboStatus = 1;
                $query->item_title_one = $request->item_title_one;
                $query->item_title_two = $request->item_title_two;


                if(isset($request->shirtSize[$key]) && $request->shirtSize[$key] != null){
                    $query->shirtSize = $request->shirtSize[$key];
                }
                if(isset($request->chest[$key])){
                    $query->chest = $request->chest[$key];
                }
                if(isset($request->shirtLength[$key])){
                    $query->shirtLength = $request->shirtLength[$key];
                }

                if(isset($request->pantSize[$key])){
                    $query->pantSize = $request->pantSize[$key];
                }
                if(isset($request->waist[$key])){
                    $query->waist = $request->waist[$key];
                }
                if(isset($request->pantLength[$key])){
                    $query->pantLength = $request->pantLength[$key];
                }

                $query->save();
            }
        }
        else{
            if(isset($request->item_title_one) && $request->item_title_one != null && isset($request->shirtSize) && $request->shirtSize != null){
                foreach($request->shirtSize as $key => $value){
                    if(isset($request->shirtSize[$key]) && $request->shirtSize[$key] != null){
                        $query = new SizeDetails();
                        $query->product_id = $product->id;
                        $query->comboStatus = 0;
                        $query->singleStatus = 1;
                        $query->item_title_one = $request->item_title_one;
                        $query->item_title_two = $request->item_title_two;
                        $query->shirtSize = $request->shirtSize[$key];

                        if(isset($request->chest[$key])){
                            $query->chest = $request->chest[$key];
                        }
                        if(isset($request->shirtLength[$key])){
                            $query->shirtLength = $request->shirtLength[$key];
                        }

                        //If pant has value
                        if(isset($request->pantSize[$key])){
                            $query->pantSize = $request->pantSize[$key];
                        }
                        if(isset($request->waist[$key])){
                            $query->waist = $request->waist[$key];
                        }
                        if(isset($request->pantLength[$key])){
                            $query->pantLength = $request->pantLength[$key];
                        }

                        $query->save();
                    }
                }
            }
            elseif(isset($request->item_title_one) && $request->item_title_one != null && isset($request->shirtSize) && $request->shirtSize != null){
                foreach($request->pantSize as $key => $value){
                    if(isset($request->pantSize[$key]) && $request->pantSize[$key] != null){
                        $query = new SizeDetails();
                        $query->product_id = $product->id;
                        $query->comboStatus = 0;
                        $query->singleStatus = 2;
                        $query->item_title_one = $request->item_title_one;
                        $query->item_title_two = $request->item_title_two;
                        $query->pantSize = $request->pantSize[$key];

                        if(isset($request->waist[$key])){
                            $query->waist = $request->waist[$key];
                        }
                        if(isset($request->pantLength[$key])){
                            $query->pantLength = $request->pantLength[$key];
                        }
                        $query->save();
                    }
                }
            }
        }



        //VAT & Tax
        if($request->tax_id) {
            foreach ($request->tax_id as $key => $val) {
                $product_tax = new ProductTax;
                $product_tax->tax_id = $val;
                $product_tax->product_id = $product->id;
                $product_tax->tax = $request->tax[$key];
                $product_tax->tax_type = $request->tax_type[$key];
                $product_tax->save();
            }
        }
        //Flash Deal
        if($request->flash_deal_id) {
            $flash_deal_product = new FlashDealProduct;
            $flash_deal_product->flash_deal_id = $request->flash_deal_id;
            $flash_deal_product->product_id = $product->id;
            $flash_deal_product->discount = $request->flash_discount;
            $flash_deal_product->discount_type = $request->flash_discount_type;
            $flash_deal_product->save();
        }

        //combinations start
        $options = array();
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0) {
            $colors_active = 1;
            array_push($options, $request->colors);
        }

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;
                $data = array();
                foreach (json_decode($request[$name][0]) as $key => $item) {
                    array_push($data, $item->value);
                }
                array_push($options, $data);
            }
        }

        //Generates the combinations of customer choice options
        $combinations = Combinations::makeCombinations($options);
        if(count($combinations[0]) > 0){
            $product->variant_product = 1;
            foreach ($combinations as $key => $combination){
                $str = '';
                foreach ($combination as $key => $item){
                    if($key > 0 ){
                        $str .= '-'.str_replace(' ', '', $item);
                    }
                    else{
                        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
                            $color_name = \App\Color::where('code', $item)->first()->name;
                            $str .= $color_name;
                        }
                        else{
                            $str .= str_replace(' ', '', $item);
                        }
                    }
                }
                $product_stock = ProductStock::where('product_id', $product->id)->where('variant', $str)->first();
                if($product_stock == null){
                    $product_stock = new ProductStock;
                    $product_stock->product_id = $product->id;
                }

                $product_stock->variant = $str;
                $product_stock->price = $request['price_'.str_replace('.', '_', $str)];
                $product_stock->sku = $request['sku_'.str_replace('.', '_', $str)];
                $product_stock->qty = $request['qty_'.str_replace('.', '_', $str)];
                $product_stock->image = $request['img_'.str_replace('.', '_', $str)];
                $product_stock->save();
            }
        }
        else{
            $product_stock              = new ProductStock;
            $product_stock->product_id  = $product->id;
            $product_stock->variant     = '';
            $product_stock->price       = $request->unit_price;
            $product_stock->sku         = $request->sku;
            $product_stock->qty         = $request->current_stock;
            $product_stock->save();
        }
        //combinations end

	    $product->save();

        // Product Translations
        $product_translation = ProductTranslation::firstOrNew(['lang' => env('DEFAULT_LANGUAGE'), 'product_id' => $product->id]);
        $product_translation->name = $request->name;
        $product_translation->unit = $request->unit;
        $product_translation->description = $request->description;
        $product_translation->save();

        flash(translate('Product has been inserted successfully'))->success();

        Artisan::call('view:clear');
        Artisan::call('cache:clear');

        if(Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'staff'){
            return redirect()->route('products.admin');
        }
        else{
            if(\App\Addon::where('unique_identifier', 'seller_subscription')->first() != null && \App\Addon::where('unique_identifier', 'seller_subscription')->first()->activated){
                $seller = Auth::user()->seller;
                $seller->remaining_uploads -= 1;
                $seller->save();
            }
            return redirect()->route('seller.products');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function admin_product_edit(Request $request, $id)
     {
        $product = Product::findOrFail($id);
        $lang = $request->lang;
        $tags = json_decode($product->tags);
        $categories = Category::where('parent_id', 0)
            ->where('digital', 0)
            ->with('childrenCategories')
            ->get();

        //Get size details.........
        $sizeDetails = DB::table('size_details')->where('product_id', $id)->get();
        $item_title = DB::table('size_details')->where('product_id', $id)->first();

        $comboStatus = 0;
        foreach($sizeDetails as $details){
            if($details->comboStatus == 1){
                $comboStatus = 1;
            }
        }

        return view('backend.product.products.edit', compact('product', 'categories', 'tags','lang','sizeDetails','comboStatus','item_title'));
     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function seller_product_edit(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $lang = $request->lang;
        $tags = json_decode($product->tags);
        $categories = Category::all();

        return view('backend.product.products.edit', compact('product', 'categories', 'tags','lang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $refund_request_addon       = \App\Addon::where('unique_identifier', 'refund_request')->first();
        $product                    = Product::findOrFail($id);
        $product->category_id       = $request->category_id;
        //$product->brand_id          = $request->brand_id;
        //$product->current_stock     = $request->current_stock;
        $product->barcode           = $request->barcode;
        $product->cash_on_delivery = 0;
        $product->featured = 0;
        $product->todays_deal = 0;
        $product->is_quantity_multiplied = 0;


        if ($refund_request_addon != null && $refund_request_addon->activated == 1) {
            if ($request->refundable != null) {
                $product->refundable = 1;
            }
            else {
                $product->refundable = 0;
            }
        }

        if($request->lang == env("DEFAULT_LANGUAGE")){
            $product->name          = $request->name;
            $product->unit          = $request->unit;
            $product->description   = $request->description;
            $product->slug          = strtolower($request->slug);
        }

        $product->photos                 = $request->photos;
        $product->measurement_image      = $request->measurement_image;
        $product->thumbnail_img          = $request->thumbnail_img;
        $product->min_qty                = $request->min_qty;
        $product->low_stock_quantity     = $request->low_stock_quantity;
        $product->priority               = $request->priority;
        $product->stock_visibility_state = $request->stock_visibility_state;

        $tags = array();
        if($request->tags[0] != null){
            foreach (json_decode($request->tags[0]) as $key => $tag) {
                array_push($tags, $tag->value);
            }
        }
        $product->tags           = implode(',', $tags);

        $product->video_provider = $request->video_provider;
        $product->video_link     = $request->video_link;
        $product->unit_price     = $request->unit_price;
        //$product->purchase_price = $request->purchase_price;
        //$product->tax            = $request->tax;
        //$product->tax_type       = $request->tax_type;
        $product->discount       = $request->discount;
        $product->shipping_type  = $request->shipping_type;
        $product->est_shipping_days  = $request->est_shipping_days;
        if ($request->has('shipping_type')) {
            if($request->shipping_type == 'free'){
                $product->shipping_cost = 0;
            }
            elseif ($request->shipping_type == 'flat_rate') {
                $product->shipping_cost = $request->flat_shipping_cost;
            }
            elseif ($request->shipping_type == 'product_wise') {
                $product->shipping_cost = json_encode($request->shipping_cost);
            }
        }

        if ($request->has('is_quantity_multiplied')) {
            $product->is_quantity_multiplied = 1;
        }
        if ($request->has('cash_on_delivery')) {
            $product->cash_on_delivery = 1;
        }

        if ($request->has('featured')) {
            $product->featured = 1;
        }

        if ($request->has('todays_deal')) {
            $product->todays_deal = 1;
        }

        $product->discount_type     = $request->discount_type;
        $product->meta_title        = $request->meta_title;
        $product->meta_description  = $request->meta_description;
        $product->meta_img          = $request->meta_img;

        if($product->meta_title == null) {
            $product->meta_title = $product->name;
        }

        if($product->meta_description == null) {
            $product->meta_description = strip_tags($product->description);
        }

        if($product->meta_img == null) {
            $product->meta_img = $product->thumbnail_img;
        }

        $product->pdf = $request->pdf;

        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $product->colors = json_encode($request->colors);
        }
        else {
            $colors = array();
            $product->colors = json_encode($colors);
        }

        $choice_options = array();

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $str = 'choice_options_'.$no;

                $item['attribute_id'] = $no;

                $data = array();
                foreach (json_decode($request[$str][0]) as $key => $eachValue) {
                    array_push($data, $eachValue->value);
                }

                $item['values'] = $data;
                array_push($choice_options, $item);
            }
        }

        foreach ($product->stocks as $key => $stock) {
            $stock->delete();
        }

        if (!empty($request->choice_no)) {
            $product->attributes = json_encode($request->choice_no);
        }
        else {
            $product->attributes = json_encode(array());
        }

        $product->choice_options = json_encode($choice_options, JSON_UNESCAPED_UNICODE);


        //combinations start
        $options = array();
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $colors_active = 1;
            array_push($options, $request->colors);
        }

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;
                $data = array();
                foreach (json_decode($request[$name][0]) as $key => $item) {
                    array_push($data, $item->value);
                }
                array_push($options, $data);
            }
        }

        $combinations = Combinations::makeCombinations($options);
        if(count($combinations[0]) > 0){
            $product->variant_product = 1;
            foreach ($combinations as $key => $combination){
                $str = '';
                foreach ($combination as $key => $item){
                    if($key > 0 ){
                        $str .= '-'.str_replace(' ', '', $item);
                    }
                    else{
                        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
                            $color_name = \App\Color::where('code', $item)->first()->name;
                            $str .= $color_name;
                        }
                        else{
                            $str .= str_replace(' ', '', $item);
                        }
                    }
                }

                $product_stock = ProductStock::where('product_id', $product->id)->where('variant', $str)->first();
                if($product_stock == null){
                    $product_stock = new ProductStock;
                    $product_stock->product_id = $product->id;
                }

                $product_stock->variant = $str;
                $product_stock->price = $request['price_'.str_replace('.', '_', $str)];
                $product_stock->sku = $request['sku_'.str_replace('.', '_', $str)];
                $product_stock->qty = $request['qty_'.str_replace('.', '_', $str)];
                $product_stock->image = $request['img_'.str_replace('.', '_', $str)];

                $product_stock->save();
            }
        }
        else{
            $product_stock              = new ProductStock;
            $product_stock->product_id  = $product->id;
            $product_stock->variant     = '';
            $product_stock->price       = $request->unit_price;
            $product_stock->sku         = $request->sku;
            $product_stock->qty         = $request->current_stock;
            $product_stock->save();
        }

        $product->save();


        // Delete previous size details
        DB::table('size_details')->where('product_id', $product->id)->delete();


        //Update size details
        if($request->has('comboPack')){
            $sizeName = '';
            if(count($request->shirtSize) > count($request->pantSize)){
                $sizeName = 'shirtSize';
            }else{
                $sizeName = 'pantSize';
            }
            foreach($request->$sizeName as $key => $value){
                $query = new SizeDetails();
                $query->product_id = $product->id;
                $query->item_title_one = $request->item_title_one;
                $query->item_title_two = $request->item_title_two;
                $query->comboStatus = 1;

                if(isset($request->shirtSize[$key]) && $request->shirtSize[$key] != null){
                    $query->shirtSize = $request->shirtSize[$key];
                }
                if(isset($request->chest[$key])){
                    $query->chest = $request->chest[$key];
                }
                if(isset($request->shirtLength[$key])){
                    $query->shirtLength = $request->shirtLength[$key];
                }

                if(isset($request->pantSize[$key])){
                    $query->pantSize = $request->pantSize[$key];
                }
                if(isset($request->waist[$key])){
                    $query->waist = $request->waist[$key];
                }
                if(isset($request->pantLength[$key])){
                    $query->pantLength = $request->pantLength[$key];
                }

                $query->save();
            }
        }
        else{
            if(isset($request->item_title_one) && $request->item_title_one != null && isset($request->shirtSize) && $request->shirtSize != null){
                foreach($request->shirtSize as $key => $value){
                    if(isset($request->shirtSize[$key]) && $request->shirtSize[$key] != null){
                        $query = new SizeDetails();
                        $query->product_id = $product->id;
                        $query->comboStatus = 0;
                        $query->singleStatus = 1;
                        $query->item_title_one = $request->item_title_one;
                        $query->item_title_two = $request->item_title_two;
                        $query->shirtSize = $request->shirtSize[$key];

                        if(isset($request->chest[$key])){
                            $query->chest = $request->chest[$key];
                        }
                        if(isset($request->shirtLength[$key])){
                            $query->shirtLength = $request->shirtLength[$key];
                        }

                        //If pant has value
                        if(isset($request->pantSize[$key])){
                            $query->pantSize = $request->pantSize[$key];
                        }
                        if(isset($request->waist[$key])){
                            $query->waist = $request->waist[$key];
                        }
                        if(isset($request->pantLength[$key])){
                            $query->pantLength = $request->pantLength[$key];
                        }

                        $query->save();
                    }
                }
            }
            elseif(isset($request->item_title_two) && $request->item_title_two != null && isset($request->pantSize) && $request->pantSize != null){
                //return $request->pantSize;
                foreach($request->pantSize as $key => $value){
                    if(isset($request->pantSize[$key]) && $request->pantSize[$key] != null){
                        $query = new SizeDetails();
                        $query->product_id = $product->id;
                        $query->comboStatus = 0;
                        $query->singleStatus = 2;
                        $query->item_title_one = $request->item_title_one;
                        $query->item_title_two = $request->item_title_two;
                        $query->pantSize = $request->pantSize[$key];

                        if(isset($request->waist[$key])){
                            $query->waist = $request->waist[$key];
                        }
                        if(isset($request->pantLength[$key])){
                            $query->pantLength = $request->pantLength[$key];
                        }
                        $query->save();
                    }
                }
            }
        }

        //Flash Deal
        if($request->flash_deal_id) {
            if($product->flash_deal_product){
                $flash_deal_product = FlashDealProduct::findOrFail($product->flash_deal_product->id);
            } if(!$flash_deal_product) {
                $flash_deal_product = new FlashDealProduct;
            }
            $flash_deal_product->flash_deal_id = $request->flash_deal_id;
            $flash_deal_product->product_id = $product->id;
            $flash_deal_product->discount = $request->flash_discount;
            $flash_deal_product->discount_type = $request->flash_discount_type;
            $flash_deal_product->save();
        }

        //VAT & Tax
        if($request->tax_id) {
            ProductTax::where('product_id', $product->id)->delete();
            foreach ($request->tax_id as $key => $val) {
                $product_tax = new ProductTax;
                $product_tax->tax_id = $val;
                $product_tax->product_id = $product->id;
                $product_tax->tax = $request->tax[$key];
                $product_tax->tax_type = $request->tax_type[$key];
                $product_tax->save();
            }
        }

        // Product Translations
        $product_translation                = ProductTranslation::firstOrNew(['lang' => $request->lang, 'product_id' => $product->id]);
        $product_translation->name          = $request->name;
        $product_translation->unit          = $request->unit;
        $product_translation->description   = $request->description;
        $product_translation->save();

        flash(translate('Product has been updated successfully'))->success();

        Artisan::call('view:clear');
        Artisan::call('cache:clear');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        foreach ($product->product_translations as $key => $product_translations) {
            $product_translations->delete();
        }

        foreach ($product->stocks as $key => $stock) {
            $stock->delete();
        }

        // Delete previous size details
        $preProductSize = DB::table('size_details')->where('product_id', $product->id)->get();
        if(count($preProductSize) > 0){
            DB::table('size_details')->where('product_id', $product->id)->delete();
        }

        if(Product::destroy($id)){

            flash(translate('Product has been deleted successfully'))->success();

            Artisan::call('view:clear');
            Artisan::call('cache:clear');

            if(Auth::user()->user_type == 'admin'){
                return redirect()->route('products.admin');
            }
            else{
                return redirect()->route('seller.products');
            }
        }
        else{
            flash(translate('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Duplicates the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function duplicate(Request $request, $id)
    {
        $original_product = Product::find($id);
        $product_new = $original_product->replicate();
        $product_new->slug = substr($product_new->slug, 0, -5).Str::random(5);
        if($product_new->save()){
            $productStock=ProductStock::where('product_id', $original_product->id)->get();
            foreach ($productStock as $key => $stock) {
                $product_stock              = new ProductStock;
                $product_stock->product_id  = $product_new->id;
                $product_stock->variant     = $stock->variant;
                $product_stock->price       = $stock->price;
                $product_stock->qty         = $stock->qty;
                $product_stock->sku         = $stock->sku;
                $product_stock->image       = $stock->image;
                $product_stock->save();

            }
        //Size details
        $sizeDetails=SizeDetails::where('product_id', $original_product->id)->get();
            foreach($sizeDetails as $key => $value){
                $query = new SizeDetails();
                $query->product_id = $product_new->id;
                $query->shirtSize = $value->shirtSize;
                $query->chest = $value->chest;
                $query->shirtLength = $value->shirtLength;
                $query->pantSize = $value->pantSize;
                $query->waist = $value->waist;
                $query->pantLength = $value->pantLength;
                $query->item_title_one = $value->item_title_one;
                $query->item_title_two = $value->item_title_two;
                $query->comboStatus = $value->comboStatus;
                $query->singleStatus = $value->singleStatus;
                $query->save();
            }

        //VAT & Tax
        $tax=ProductTax::where('product_id', $original_product->id)->get();
        if(isset($tax)) {
            foreach ($tax as $key => $val) {
                $product_tax = new ProductTax;
                $product_tax->tax_id = $val->id;
                $product_tax->product_id = $product_new->id;
                $product_tax->tax = $val->tax;
                $product_tax->tax_type = $val->tax_type;
                $product_tax->save();
            }
        }
        //Flash Deal
        $flashDealProduct=FlashDealProduct::where('product_id', $original_product->id)->first();
        if(isset($flashDealProduct)) {
            $flash_deal_product = new FlashDealProduct;
            $flash_deal_product->flash_deal_id = $flashDealProduct->flash_deal_id;
            $flash_deal_product->product_id = $product_new->id;
            $flash_deal_product->discount = $flashDealProduct->flash_discount;
            $flash_deal_product->discount_type = $flashDealProduct->flash_discount_type;
            $flash_deal_product->save();
        }
        //END
        Artisan::call('view:clear');
        Artisan::call('cache:clear');
            flash(translate('Product has been duplicated successfully'))->success();
            if(Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'staff'){
              if($request->type == 'In House')
                return redirect()->route('products.admin');
              elseif($request->type == 'Seller')
                return redirect()->route('products.seller');
              elseif($request->type == 'All')
                return redirect()->route('products.all');
            }
            else{
                return redirect()->route('seller.products');
            }
        }
        else{
            flash(translate('Something went wrong'))->error();
            return back();
        }
    }

    public function get_products_by_brand(Request $request)
    {
        $products = Product::where('brand_id', $request->brand_id)->get();
        return view('partials.product_select', compact('products'));
    }

    public function updateTodaysDeal(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $product->todays_deal = $request->status;
        if($product->save()){
            return 1;
        }
        return 0;
    }

    public function updatePublished(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $product->published = $request->status;

        if($product->added_by == 'seller' && \App\Addon::where('unique_identifier', 'seller_subscription')->first() != null && \App\Addon::where('unique_identifier', 'seller_subscription')->first()->activated){
            $seller = $product->user->seller;
            if($seller->invalid_at != null && Carbon::now()->diffInDays(Carbon::parse($seller->invalid_at), false) <= 0){
                return 0;
            }
        }

        $product->save();
        return 1;
    }

    public function updateFeatured(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $product->featured = $request->status;
        if($product->save()){
            return 1;
        }
        return 0;
    }

    public function updateSellerFeatured(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $product->seller_featured = $request->status;
        if($product->save()){
            return 1;
        }
        return 0;
    }

    public function sku_combination(Request $request)
    {
        $options = array();
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $colors_active = 1;
            array_push($options, $request->colors);
        }
        else {
            $colors_active = 0;
        }

        $unit_price = $request->unit_price;
        $product_name = $request->name;

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;
                $data = array();
                foreach (json_decode($request[$name][0]) as $key => $item) {
                    array_push($data, $item->value);
                }
                array_push($options, $data);
            }
        }

        $combinations = Combinations::makeCombinations($options);
        return view('backend.product.products.sku_combinations', compact('combinations', 'unit_price', 'colors_active', 'product_name'));
    }

    public function sku_combination_edit(Request $request)
    {
        $product = Product::findOrFail($request->id);

        $options = array();
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $colors_active = 1;
            array_push($options, $request->colors);
        }
        else {
            $colors_active = 0;
        }

        $product_name = $request->name;
        $unit_price = $request->unit_price;

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;
                $data = array();
                foreach (json_decode($request[$name][0]) as $key => $item) {
                    array_push($data, $item->value);
                }
                array_push($options, $data);
            }
        }

        $combinations = Combinations::makeCombinations($options);
        return view('backend.product.products.sku_combinations_edit', compact('combinations', 'unit_price', 'colors_active', 'product_name', 'product'));
    }


    public function getSizeDetails(Request $request){
        if(($request->size !== null) && ($request->color !== null)){
            $size = $request->size;
            $color = $request->color;
            $variant = $size.'-'.$color;
        }else{
            $color = $request->color;
            $variant = $request->size;
        }
        $product = Product::find($request->product_id);
        $product_stock = $product->stocks->where('variant', $variant)->first();
        $sku = $product_stock->sku;


        $sizeDetails = DB::table('size_details')->where('product_id', $request->product_id)->first();

        $query = DB::table('size_details');
        $query->where('product_id', $request->product_id);

        if($sizeDetails->comboStatus == 1){

            if(isset($sizeDetails->shirtSize) && $sizeDetails->shirtSize != null){
                $query->where('shirtSize', $request->size);
            }else{
                $query->where('pantSize', $request->size);
            }
        }
        elseif($sizeDetails->singleStatus == 1){
            $query->where('shirtSize', $request->size);
        }
        elseif($sizeDetails->singleStatus == 2){
            $query->where('pantSize', $request->size);
        }

        $sizeDescription = $query->get();
        //return $sizeDescription;
        return response()->json([
            'sku' => $sku,
            'sizeDescription' => $sizeDescription
        ]);
    }

}
