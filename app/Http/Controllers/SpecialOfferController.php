<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Coupon;
use App\SpecialOffer;

class SpecialOfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $special_offers = SpecialOffer::orderBy('id','desc')->get();
        return view('backend.conditional_sale.special_offer.index',compact('special_offers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.conditional_sale.special_offer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['status'] = true;
        $date_var                 = explode(" - ", $request->date_range);
        $data['start_date'] = strtotime($date_var[0]);
        $data['end_date'] = strtotime($date_var[1]);

        if($request->has('product_ids')){
            $data['product_ids'] = json_encode($request->product_ids);
        }

        if($request->has('min_qty_Price_break')){
            $data['min_qty_Price_break'] = json_encode($request->min_qty_Price_break);
            $data['offer_product_ids'] = '';
        }

        if($request->has('discount_Price_break')){
            $data['discount_Price_break'] = json_encode($request->discount_Price_break);
        }

        if($request->has('offer_product_ids')){
            $data['offer_product_ids'] = $request->offer_product_ids;
        }

        try{
            SpecialOffer::create($data);
            $message = 'Special Offer Created Successfully';
        }
        catch(\Exception $e){
            $message = 'Something error found';
        }
        flash(translate('Special Offer has been saved successfully'))->success();
        return redirect()->route('special_offer.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $offer = SpecialOffer::findOrFail(decrypt($id));
        return view('backend.conditional_sale.special_offer.edit', compact('offer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return $request->all();
        $data = $request->all();
        $data['status'] = true;
        $date_var                 = explode(" - ", $request->date_range);
        $data['start_date'] = strtotime($date_var[0]);
        $data['end_date'] = strtotime($date_var[1]);

        if($request->has('product_ids')){
            $data['product_ids'] = json_encode($request->product_ids);
        }

        if($request->has('min_qty_Price_break')){
            $data['min_qty_Price_break'] = json_encode($request->min_qty_Price_break);
            $data['offer_product_ids'] = '';
        }

        if($request->has('discount_Price_break')){
            $data['discount_Price_break'] = json_encode($request->discount_Price_break);
        }

        if($request->has('offer_product_ids')){
            $data['offer_product_ids'] = $request->offer_product_ids;
        }

        try{
            $special = SpecialOffer::findOrFail($id);
            $special->update($data);
            $message = 'Special Offer Updated Successfully';
        }
        catch(\Exception $e){
            $message = 'Something error found';
            flash(translate('Something went wrong'))->danger();
            return back();
        }
        flash(translate('Special Offer has been saved successfully'))->success();
        return redirect()->route('special_offer.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $special = SpecialOffer::findOrFail($id);
            $special->delete();
            $message = 'Special Offer Deleted Successfully';
        }
        catch(\Exception $e){
            $message = 'Something error found';
            flash(translate('Something went wrong'))->danger();
            return back();
        }
        flash(translate('Special Offer Deleted Successfully'))->success();
        return redirect()->route('special_offer.index');
    }

    public function get_offer_form(Request $request)
    {
        if($request->offer_type == "product_base") {
            return view('backend.conditional_sale.special_offer.product_base_coupon');
        }
        elseif($request->offer_type == "cart_base"){
            return view('backend.conditional_sale.special_offer.cart_base_coupon');
        }
    }

    public function get_offer_form_edit(Request $request){
        //return $request->all();
        if($request->offer_type == "product_base") {
            $offer = SpecialOffer::findOrFail($request->id);
            return view('backend.conditional_sale.special_offer.product_base_coupon_edit',compact('offer'));
        }
        elseif($request->offer_type == "cart_base"){
            //return $request->offer_type;
            $offer = SpecialOffer::findOrFail($request->id);
            return view('backend.conditional_sale.special_offer.cart_base_coupon_edit',compact('offer'));
        }
    }

    public function updatePublished(Request $request)
    {
        $offer = SpecialOffer::findOrFail($request->id);
        $offer->status = $request->status;

        $offer->save();
        return 1;
    }
}
