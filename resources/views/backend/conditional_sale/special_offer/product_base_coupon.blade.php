<div class="card-header mb-2">
    <h3 class="h6">{{translate('Add Your Product Base Special Offer')}}</h3>
</div>

<div class="form-group row">
    <label class="col-lg-3 col-from-label" for="name">{{translate('Discount Type')}}</label>
    <div class="col-lg-9">
        <select name="discount_method" id="discount_method" class="form-control aiz-selectpicker" required>
            <option value="">{{translate('Select One') }}</option>
            <option value="buy_get_offer">{{translate('For Buy Get')}}</option>
            <option value="quantity_offer">{{translate('For Free Different Gift On Particular Product')}}</option>
            <option value="flat_discount_offer">{{translate('For Flat/Parcentage Discount On Particular Amount(Tk)')}}</option>
            <option value="discount_on_quantiry">{{translate('For Flat/Parcentage Discount On Quantity')}}</option>
            <option value="price_break_discount">{{translate('For Price-Break Discount (Different Discount On Different Quantity)')}}</option>
        </select>
    </div>
</div>

<div class="form-group row" id="offer_title">
    <label class="col-lg-3 col-from-label" for="coupon_code">{{translate('Special Offer Title')}}</label>
    <div class="col-lg-9">
        <input type="text" placeholder="{{translate('Special Offer Title')}}" id="coupon_code" name="title" class="form-control" required>
    </div>
</div>
<div class="product-choose-list" id="product">
    <div class="product-choose">
        <div class="form-group row">
            <label class="col-lg-3 col-from-label" for="name">{{translate('Product')}}</label>
            <div class="col-lg-9">
                <select name="product_ids[]" class="form-control product_id aiz-selectpicker" data-live-search="true" data-selected-text-format="count" multiple>
                    @foreach(filter_products(\App\Product::query())->get() as $product)
                        <option value="{{$product->id}}">{{ $product->getTranslation('name') }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>

<div class="card" id="quantity_discount">
    <div class="card-body">
        <div class="size_details_options" id="size_details_options">
            <div class="form-group row">
                <label class="col-md-5 col-from-label">Quantity </label>
                <label class="col-md-5 col-from-label">Discount </label>
            </div>
            <div class="form-group row" id="shirtDetailsInput">
                <div class="col-md-5">
                    <input type="text" placeholder="Quantity" name="min_qty_Price_break[]" class="form-control">
                </div>
                <div class="col-md-5">
                    <input type="string" placeholder="Discount" name="discount_Price_break[]" class="form-control">
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-sm btn-primary" id="add_more_shirt">Add</button>
                </div>
            </div>

            <div class="form-group row" id="newShirtRow">

            </div>
        </div>
    </div>
</div>

<div class="product-choose-list" id="offered_product">
    <div class="product-choose">
        <div class="form-group row">
            <label class="col-lg-3 col-from-label" for="name">{{translate('Offered Product')}}</label>
            <div class="col-lg-9">
                <select name="offer_product_ids" class="form-control product_id aiz-selectpicker" data-live-search="true" data-selected-text-format="count">
                    <option value="">Select Product</option>
                    @foreach(filter_products(\App\Product::query())->get() as $product)
                        <option value="{{$product->id}}">{{ $product->getTranslation('name') }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>

<div class="form-group row" id="min_shopping">
    <label class="col-lg-3 col-from-label">{{translate('Minimum Shopping')}}</label>
    <div class="col-lg-9">
       <input type="number" lang="en" min="0" step="0.01" placeholder="{{translate('Minimum Shopping')}}" name="min_buy" class="form-control">
    </div>
 </div>

 <div class="form-group row" id="shopping_quantity">
    <label class="col-lg-3 col-from-label">{{translate('Minimum Quantity')}}</label>
    <div class="col-lg-9">
       <input type="number" lang="en" min="0" step="0.01" placeholder="{{translate('Minimum Qty')}}" name="min_qty" class="form-control">
    </div>
 </div>

 <div class="form-group row" id="discount_qty">
    <label class="col-lg-3 col-from-label">{{translate('Discount Quantity')}}</label>
    <div class="col-lg-9">
       <input type="number" lang="en" min="0" step="0.01" placeholder="{{translate('Minimum Shopping Qty')}}" name="discount_qty" class="form-control">
    </div>
 </div>

<div class="form-group row" id="discount">
    <label class="col-lg-3 col-from-label">{{translate('Discount')}}</label>
    <div class="col-lg-7">
       <input type="number" lang="en" min="0" step="0.01" placeholder="{{translate('Discount')}}" name="discount" class="form-control">
    </div>
    <div class="col-lg-2">
        <select class="form-control aiz-selectpicker" name="discount_type">
            <option value="amount">{{translate('Amount')}}</option>
            <option value="percent">{{translate('Percent')}}</option>
        </select>
    </div>
 </div>

 <div class="form-group row" id="max_discount">
    <label class="col-lg-3 col-from-label">{{translate('Maximum Discount Amount')}}</label>
    <div class="col-lg-9">
       <input type="number" lang="en" min="0" step="0.01" placeholder="{{translate('Maximum Discount Amount')}}" name="max_discount" class="form-control">
    </div>
 </div>

<div class="form-group row" id="date">
    <label class="col-sm-3 control-label" for="start_date">{{translate('Date')}}</label>
    <div class="col-sm-9">
      <input type="text" class="form-control aiz-date-range" name="date_range" placeholder="Select Date">
    </div>
</div>


<script type="text/javascript">
  $('#offer_title').hide();
  $('#product').hide();
  $('#quantity_discount').hide();
  $('#offered_product').hide();
  $('#date').hide();
  $('#discount').hide();
  $('#discount_qty').hide();
// add row for shirt
    $("#add_more_shirt").click(function () {
        var html = '';
        html += '<div class="form-group row" id="shirtDetailsInput">';
        html += '<div class="col-md-5">';
        html += '<input type="text" placeholder="Quantity" name="min_qty_Price_break[]" class="form-control">';
        html += '</div>';
        html += '<div class="col-md-5">';
        html += '<input type="string" placeholder="Discount" name="discount_Price_break[]" class="form-control">';
        html += '</div>';
        html += '<div class="col-md-2">';
        html += '<button id="removeShirtRow" type="button" class="btn btn-danger"><i class="las la-trash-alt"></i></button>';
        html += '</div>';
        html += '</div>';

        $('#newShirtRow').append(html);
    });

    // remove row
    $(document).on('click', '#removeShirtRow', function () {
        $(this).closest('#shirtDetailsInput').remove();
    });

    $(document).ready(function(){
        $('.aiz-date-range').daterangepicker();
        AIZ.plugins.bootstrapSelect('refresh');
    });

    $('select[name="discount_method"]').on('change', function() {
         if ($(this).val() == 'buy_get_offer') {
            $('#discount').hide();
            $('#quantity_discount').hide();
            $('#min_shopping').hide();
            $('#shopping_quantity').hide();
            $('#max_discount').hide();
            $('#discount_qty').hide();

            $('#offer_title').show(500);
            $('#product').show(500);
            $('#offered_product').show(500);
            $('#date').show(500);
         } else if($(this).val() == 'quantity_offer'){
            $('#quantity_discount').hide();
            $('#min_shopping').hide();
            $('#max_discount').hide();

            $('#offer_title').show(500);
            $('#product').show(500);
            $('#date').show(500);
            $('#discount_qty').show(500);
         }else if($(this).val() == 'price_break_discount'){
            $('#discount').hide();
            $('#offered_product').hide();
            $('#quantity_discount').hide();
            $('#discount_qty').hide();

            $('#offer_title').show(500);
            $('#product').show(500);
            $('#date').show(500);
            $('#quantity_discount').show(500);

         }else if($(this).val() == 'flat_discount_offer'){
            $('#shopping_quantity').hide();
            $('#offered_product').hide();
            $('#quantity_discount').hide();
            $('#discount_qty').hide();

            $('#offer_title').show(500);
            $('#product').show(500);
            $('#date').show(500);
            $('#discount').show(500);
            $('#min_shopping').show(500);
            $('#max_discount').show(500);

         }else if($(this).val() == 'discount_on_quantiry'){
            $('#min_shopping').hide();
            $('#quantity_discount').hide();
            $('#discount_qty').hide();

            $('#offer_title').show(500);
            $('#product').show(500);
            $('#date').show(500);
            $('#discount').show(500);
            $('#shopping_quantity').show(500);

         }
    });

</script>
