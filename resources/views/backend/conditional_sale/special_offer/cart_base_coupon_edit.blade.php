{{-- @php
    $coupon_det = json_decode($coupon->details);
@endphp --}}

<div class="card-header mb-2">
   <h3 class="h6">{{translate('Edit Your Cart Base Offer')}}</h3>
</div>

<div class="form-group row">
    <label class="col-lg-3 col-from-label" for="name">{{translate('Discount Type')}}</label>
    <div class="col-lg-9">
        <select name="discount_method" id="discount_method" class="form-control aiz-selectpicker" required>
            <option value="">{{translate('Select One') }}</option>
            <option value="quantity_offer" {{ ($offer->discount_method == 'quantity_offer') ? 'selected':'' }}>{{translate('For Same Product Gift/Free On Particular Quantity')}}</option>
            <option value="flat_discount_offer" {{ ($offer->discount_method == 'flat_discount_offer') ? 'selected':'' }}>{{translate('For Flat/Parcentage Discount On Particular Amount(Tk)')}}</option>
            <option value="delivery_chare_on_quantity" {{ ($offer->discount_method == 'delivery_chare_on_quantity') ? 'selected':'' }}>{{translate('For Delivery Charge Free On Particular Quantity')}}</option>
            <option value="delivery_chare_on_amount" {{ ($offer->discount_method == 'delivery_chare_on_amount') ? 'selected':'' }}>{{translate('For Delivery Charge Free On Particular Amount(Tk)')}}</option>
            <option value="discount_on_quantiry" {{ ($offer->discount_method == 'discount_on_quantiry') ? 'selected':'' }}>{{translate('For Flat/Parcentage Discount On Quantity')}}</option>
        </select>
    </div>
</div>


<div class="form-group row">
   <label class="col-lg-3 col-from-label" for="coupon_code">{{translate('Special Offer Title')}}</label>
   <div class="col-lg-9">
       <input type="text" value="{{$offer->title}}" id="coupon_code" name="title" class="form-control" required>
   </div>
</div>


<div class="form-group row" id="min_shopping">
  <label class="col-lg-3 col-from-label">{{translate('Minimum Shopping')}}</label>
  <div class="col-lg-9">
     <input type="number" lang="en" min="0" step="0.01" name="min_buy" class="form-control" value="{{ $offer->min_buy }}">
  </div>
</div>
<div class="form-group row" id="discount">
   <label class="col-lg-3 col-from-label">{{translate('Discount')}}</label>
   <div class="col-lg-7">
       <input type="number" lang="en" min="0" step="0.01" placeholder="{{translate('Discount')}}" name="discount" class="form-control" value="{{ $offer->discount }}">
   </div>
   <div class="col-lg-2">
       <select class="form-control aiz-selectpicker" name="discount_type">
           <option value="amount" @if ($offer->discount_type == 'amount') selected  @endif >{{translate('Amount')}}</option>
           <option value="percent" @if ($offer->discount_type == 'percent') selected  @endif>{{translate('Percent')}}</option>
       </select>
   </div>
</div>

<div class="form-group row" id="shopping_quantity">
    <label class="col-lg-3 col-from-label">{{translate('Minimum Quantity')}}</label>
    <div class="col-lg-9">
       <input type="number" lang="en" min="0" step="0.01" placeholder="{{translate('Minimum Qty')}}" name="min_qty" value="{{ $offer->min_qty }}" class="form-control">
    </div>
 </div>

 <div class="form-group row" id="discount_qty">
    <label class="col-lg-3 col-from-label">{{translate('Discount Quantity')}}</label>
    <div class="col-lg-9">
       <input type="number" lang="en" min="0" step="0.01" placeholder="{{translate('Minimum Shopping Qty')}}" name="discount_qty" value="{{ $offer->discount_qty }}" class="form-control">
    </div>
 </div>

<div class="form-group row" id="max_discount">
  <label class="col-lg-3 col-from-label">{{translate('Maximum Discount Amount')}}</label>
  <div class="col-lg-9">
     <input type="number" lang="en" min="0" step="0.01" placeholder="{{translate('Maximum Discount Amount')}}" name="max_discount" class="form-control" value="{{ $offer->max_discount }}">
  </div>
</div>

@php
  $start_date = date('m/d/Y', $offer->start_date);
  $end_date = date('m/d/Y', $offer->end_date);
@endphp
<div class="form-group row">
    <label class="col-sm-3 control-label" for="start_date">{{translate('Date')}}</label>
    <div class="col-sm-9">
      <input type="text" class="form-control aiz-date-range" value="{{ $start_date .' - '. $end_date }}" name="date_range" placeholder="Select Date">
    </div>
</div>


<script type="text/javascript">
   $(document).ready(function(){
       $('.aiz-selectpicker').selectpicker();
       $('.aiz-date-range').daterangepicker();
   });

   var discount_method = $('#discount_method').val();
   if(discount_method == 'quantity_offer'){
        $('#shopping_quantity').show();
        $('#discount_qty').show();

        $('#discount').hide();
        $('#min_shopping').hide();
        $('#max_discount').hide();
   }else if(discount_method == 'flat_discount_offer'){
        $('#shopping_quantity').hide();
        $('#discount_qty').hide();

        $('#discount').show();
        $('#min_shopping').show();
        $('#max_discount').show();
   }else if(discount_method == 'delivery_chare_on_quantity'){
        $('#shopping_quantity').show();
        $('#discount_qty').hide();

        $('#discount').hide();
        $('#min_shopping').hide();
        $('#max_discount').hide();
   }else if(discount_method == 'delivery_chare_on_amount'){
        $('#shopping_quantity').hide();
        $('#discount_qty').hide();
        $('#discount').hide();
        $('#max_discount').hide();
        $('#min_shopping').show();
   }else if(discount_method == 'discount_on_quantiry'){
        $('#discount').show();
        $('#shopping_quantity').show();
        $('#max_discount').show();
        $('#discount_qty').hide();
        $('#min_shopping').hide();
   }
   //alert($('#discount_method').val());

   $('select[name="discount_method"]').on('change', function() {
        if($(this).val() == 'quantity_offer'){
            $('#shopping_quantity').show(500);
            $('#discount_qty').show(500);

            $('#discount').hide(500);
            $('#min_shopping').hide(500);
            $('#max_discount').hide(500);
        }else if($(this).val() == 'flat_discount_offer'){
             $('#shopping_quantity').hide(500);
             $('#discount_qty').hide(500);

             $('#discount').show(500);
             $('#min_shopping').show(500);
             $('#max_discount').show(500);
        }else if($(this).val() == 'delivery_chare_on_quantity'){
            $('#shopping_quantity').show(500);
            $('#discount_qty').hide(500);

            $('#discount').hide(500);
            $('#min_shopping').hide(500);
            $('#max_discount').hide(500);
        }else if($(this).val() == 'delivery_chare_on_amount'){
            $('#shopping_quantity').hide(500);
            $('#discount_qty').hide(500);
            $('#discount').hide(500);
            $('#max_discount').hide(500);
            $('#min_shopping').show(500);
        }else if($(this).val() == 'discount_on_quantiry'){
            $('#discount').show(500);
            $('#shopping_quantity').show(500);
            $('#max_discount').show(500);

            $('#discount_qty').hide(500);
            $('#min_shopping').hide(500);
        }
    });

</script>
