<div class="card-header mb-2">
    <h3 class="h6">{{translate('Add Your Cart Base Offer')}}</h3>
</div>

<div class="form-group row">
    <label class="col-lg-3 col-from-label" for="name">{{translate('Discount Type')}}</label>
    <div class="col-lg-9">
        <select name="discount_method" id="discount_method" class="form-control aiz-selectpicker" required>
            <option value="">{{translate('Select One') }}</option>
            <option value="quantity_offer">{{translate('For Same Product Gift/Free On Particular Quantity')}}</option>
            <option value="flat_discount_offer">{{translate('For Flat/Parcentage Discount On Particular Amount(Tk)')}}</option>
            <option value="delivery_chare_on_quantity">{{translate('For Delivery Charge Free On Particular Quantity')}}</option>
            <option value="delivery_chare_on_amount">{{translate('For Delivery Charge Free On Particular Amount(Tk)')}}</option>
            <option value="discount_on_quantiry">{{translate('For Flat/Parcentage Discount On Quantity')}}</option>
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-lg-3 col-from-label" for="coupon_code">{{translate('Special Offer Title')}}</label>
    <div class="col-lg-9">
        <input type="text" placeholder="{{translate('Offer Name')}}" id="coupon_code" name="title" class="form-control" required>
    </div>
</div>
<div class="form-group row" id="min_shopping">
   <label class="col-lg-3 col-from-label">{{translate('Minimum Shopping')}}</label>
   <div class="col-lg-9">
      <input type="number" lang="en" min="0" step="0.01" placeholder="{{translate('Minimum Shopping')}}" name="min_buy" class="form-control">
   </div>
</div>
<div class="form-group row" id="discount">
   <label class="col-lg-3 col-from-label">{{translate('Discount')}}</label>
   <div class="col-lg-7">
      <input type="number" lang="en" min="0" step="0.01" placeholder="{{translate('Discount')}}" name="discount" class="form-control">
   </div>
   <div class="col-lg-2">
       <select class="form-control aiz-selectpicker" name="discount_type">
           <option value="amount">{{translate('Amount')}}</option>
           <option value="percent">{{translate('Percent')}}</option>
       </select>
   </div>
</div>

<div class="form-group row" id="shopping_quantity">
    <label class="col-lg-3 col-from-label">{{translate('Minimum Quantity')}}</label>
    <div class="col-lg-9">
       <input type="number" lang="en" min="0" step="0.01" placeholder="{{translate('Minimum Qty')}}" name="min_qty" class="form-control">
    </div>
 </div>

 <div class="form-group row" id="discount_qty">
    <label class="col-lg-3 col-from-label">{{translate('Discount Quantity')}}</label>
    <div class="col-lg-9">
       <input type="number" lang="en" min="0" step="0.01" placeholder="{{translate('Minimum Shopping Qty')}}" name="discount_qty" class="form-control">
    </div>
 </div>

<div class="form-group row" id="max_discount">
   <label class="col-lg-3 col-from-label">{{translate('Maximum Discount Amount')}}</label>
   <div class="col-lg-9">
      <input type="number" lang="en" min="0" step="0.01" placeholder="{{translate('Maximum Discount Amount')}}" name="max_discount" class="form-control">
   </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 control-label" for="start_date">{{translate('Date')}}</label>
    <div class="col-sm-9">
      <input type="text" class="form-control aiz-date-range" name="date_range" placeholder="Select Date">
    </div>
</div>

<script type="text/javascript">

    $('#shopping_quantity').hide();
    $('#discount_qty').hide();

    $('#discount').hide();
    $('#min_shopping').hide();
    $('#max_discount').hide();

    $(document).ready(function(){
        $('.aiz-selectpicker').selectpicker();
        $('.aiz-date-range').daterangepicker();
    });

    $('select[name="discount_method"]').on('change', function() {
        if($(this).val() == 'quantity_offer'){
            $('#shopping_quantity').show(500);
            $('#discount_qty').show(500);

            $('#discount').hide(500);
            $('#min_shopping').hide(500);
            $('#max_discount').hide(500);
        }else if($(this).val() == 'flat_discount_offer'){
             $('#shopping_quantity').hide(500);
             $('#discount_qty').hide(500);

             $('#discount').show(500);
             $('#min_shopping').show(500);
             $('#max_discount').show(500);
        }else if($(this).val() == 'delivery_chare_on_quantity'){
            $('#shopping_quantity').show(500);
            $('#discount_qty').hide(500);

            $('#discount').hide(500);
            $('#min_shopping').hide(500);
            $('#max_discount').hide(500);
        }else if($(this).val() == 'delivery_chare_on_amount'){
            $('#shopping_quantity').hide(500);
            $('#discount_qty').hide(500);
            $('#discount').hide(500);
            $('#max_discount').hide(500);
            $('#min_shopping').show(500);
        }else if($(this).val() == 'discount_on_quantiry'){
            $('#discount').show(500);
            $('#shopping_quantity').show(500);
             $('#max_discount').show(500);
        }
    });

</script>
