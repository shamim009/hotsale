@extends('backend.layouts.app')

@section('content')

<div class="row">
    <div class="col-lg-6 mx-auto">
        <div class="card">
            <div class="card-header">
                <h5 class="mb-0 h6">New Menu</h5>
            </div>

            <form class="form-horizontal" action="{{ route('menu.store') }}" method="POST">
            	@csrf
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-sm-3 col-from-label" for="name">{{translate('Name')}}</label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="{{translate('Name')}}" id="name" name="name" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-from-label" for="parent">Parent</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="parent_id" id="parent" required>
                                <option value="0">Root Menu</option>
                                {{ getMenu() }}
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-from-label" for="priority">Priority</label>
                        <div class="col-sm-9">
                            <input type="number" min="1" step="1" placeholder="Set Priority" name="priority" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-from-label">{{translate('Status')}}</label>
                        <div class="col-md-9">
                            <label class="aiz-switch aiz-switch-success mb-0">
                                <input type="checkbox" name="status" value="1" checked>
                                <span></span>                                
                            </label>
                        </div>
                    </div>
                    <div class="form-group mb-0 text-right">
                        <a href="{{ route('menu.index') }}" class="btn btn-sm btn-warning">{{translate('Cancel')}}</a>
                        <button type="submit" class="btn btn-sm btn-primary">{{translate('Save')}}</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>

@endsection
