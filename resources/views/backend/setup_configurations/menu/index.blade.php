@extends('backend.layouts.app')

@section('content')

<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="row align-items-center">
		<div class="col-md-6">
			<h1 class="h3">{{translate('All Menu')}}</h1>
		</div>
		<div class="col-md-6 text-md-right">
			<a href="{{ route('menu.create') }}" class="btn btn-circle btn-info">
				<span>{{translate('Add New Menu')}}</span>
			</a>
		</div>
	</div>
</div>

<div class="card">
    <div class="card-header">
        <h5 class="mb-0 h6">{{translate('Menu')}}</h5>
    </div>
    <div class="card-body">
        <table class="table aiz-table mb-0">
            <thead>
                <tr>
                    <th data-breakpoints="lg" width="10%">#</th>
                    <th>{{translate('Name')}}</th>
                    <th data-breakpoints="lg">Parent</th>
                    <th data-breakpoints="lg">Priority</th>
                    <th data-breakpoints="lg">Status</th>
                    <th width="10%">{{translate('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($menus as $key => $menu)
                        <tr>
                            <td>{{ ($key+1) + ($menus->currentPage() - 1)*$menus->perPage() }}</td>
                            <td>{{$menu->name}}</td>
                            <td>{{$menu->parent_id}}</td>
                            <td>{{$menu->priority}}</td>
                            <td>@if($menu->status == 1) 
                                    <span class="badge-primary">Active</span> 
                                @else
                                    <span class="badge-warning">Deactive</span>
                                @endif
                            </td>
                            <td class="text-right">
		                            <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{route('menu.edit', $menu->id)}}" title="{{ translate('Edit') }}">
		                                <i class="las la-edit"></i>
		                            </a>
		                            <a href="#" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete" data-href="{{route('menu.delete', $menu->id)}}" title="{{ translate('Delete') }}">
		                                <i class="las la-trash"></i>
		                            </a>
		                        </td>
                        </tr>
                @endforeach
            </tbody>
        </table>
        <div class="aiz-pagination">
            {{ $menus->appends(request()->input())->links() }}
        </div>
    </div>
</div>

@endsection

@section('modal')
    @include('modals.delete_modal')
@endsection
