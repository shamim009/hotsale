@extends('frontend.layouts.app')

@section('content')
    <section class="gry-bg py-5">
        <div class="profile">
            <div class="container">
                <div class="row">
                    <div class="col-xxl-4 col-xl-5 col-lg-6 col-md-8 mx-auto">
                        <div class="card">
                            <div class="text-center pt-4">
                                <h1 class="h4 fw-600">
                                    {{ translate('Login to your account.')}}
                                </h1>
                            </div>

                            <div class="px-4 py-3 py-lg-4">
                                {{--Success message--}}
                                <p class="alert alert-success alert-dismissible fade show" id="success_msg" style="display: none">
                                    <strong>Confirmation code has been sent to your phone.</strong>
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                </p>
                                {{--Error message--}}
                                <p class="alert alert-danger alert-dismissible fade show" id="error_msg" style="display: none">
                                    <strong>Error!</strong> Something went wrong,Try again.
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                </p>
                                <div class="">
                                    {{-- <form class="form-default" role="form" action="{{ route('user.otp.login') }}" method="POST"> --}}

                                        {{-- Login without otp --}}
                                    <form class="form-default" role="form" action="{{ route('user.otp.send') }}" method="POST">
                                        @csrf
                                        <div class="form-group">
                                            @if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated)
                                                <input type="text" class="form-control phone_number {{ $errors->has('phone') ? ' is-invalid' : '' }}" value="{{ old('phone', '+8801') }}" placeholder="{{ translate('Phone')}}" name="phone" id="phone_number">
                                            @else
                                                <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="{{  translate('Email') }}" name="email">
                                            @endif
                                            @if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated)
                                                <span class="opacity-60">{{  translate('Use country code before number') }}</span>
                                            @endif
                                        </div>

                                        {{-- <div class="form-group" id="otp" style="display: none">
                                            <input type="number" class="form-control" name="otp" placeholder="Enter confirmation code" required>
                                            <i class="icofont-pin"></i>
                                        </div> --}}
                                        <div><span> </span></div>

                                        <!-- <div class="form-group">
                                            <input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{ translate('Password')}}" name="password" id="password">
                                        </div> -->

                                         {{-- <div class="row mb-2">
                                            <div class="col-6">
                                                <label class="aiz-checkbox">
                                                    <input type="checkbox" name="remember" checked>
                                                    <span class=opacity-60>{{  translate('Remember Me') }}</span>
                                                    <span class="aiz-square-check"></span>
                                                </label>
                                            </div> 
                                            <div class="col-6 text-right">
                                                <a href="{{ route('password.request') }}" class="text-reset opacity-60 fs-14">{{ translate('Forgot password?')}}</a>
                                            </div>
                                        </div> --}}

                                        <div class="mb-5">
                                            <button type="submit" class="btn btn-primary btn-block submit-btn fw-600" style="background-color: gray">{{  translate('Login') }}</button>
                                            {{-- <button type="submit" onclick="return false" class="btn btn-primary btn-block submit-btn fw-600" style="background-color: gray" disabled>{{  translate('Login') }}</button> --}}
                                        </div>
                                    </form>

                                    @if (env("DEMO_MODE") == "On")
                                        <div class="mb-5">
                                            <table class="table table-bordered mb-0">
                                                <tbody>
                                                    <tr>
                                                        <td>{{ translate('Seller Account')}}</td>
                                                        <td>
                                                            <button class="btn btn-info btn-sm" onclick="autoFillSeller()">{{ translate('Copy credentials') }}</button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ translate('Customer Account')}}</td>
                                                        <td>
                                                            <button class="btn btn-info btn-sm" onclick="autoFillCustomer()">{{ translate('Copy credentials') }}</button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    @endif

                                    @if(get_setting('google_login') == 1 || get_setting('facebook_login') == 1 || get_setting('twitter_login') == 1)
                                        <div class="separator mb-3">
                                            <span class="bg-white px-3 opacity-60">{{ translate('Or Login With')}}</span>
                                        </div>
                                        <ul class="list-inline social colored text-center mb-5">
                                            @if (get_setting('facebook_login') == 1)
                                                <li class="list-inline-item">
                                                    <a href="{{ route('social.login', ['provider' => 'facebook']) }}" class="facebook">
                                                        <i class="lab la-facebook-f"></i>
                                                    </a>
                                                </li>
                                            @endif
                                            @if(get_setting('google_login') == 1)
                                                <li class="list-inline-item">
                                                    <a href="{{ route('social.login', ['provider' => 'google']) }}" class="google">
                                                        <i class="lab la-google"></i>
                                                    </a>
                                                </li>
                                            @endif
                                            @if (get_setting('twitter_login') == 1)
                                                <li class="list-inline-item">
                                                    <a href="{{ route('social.login', ['provider' => 'twitter']) }}" class="twitter">
                                                        <i class="lab la-twitter"></i>
                                                    </a>
                                                </li>
                                            @endif
                                        </ul>
                                    @endif
                                </div>
                                <div class="text-center">
                                    <p class="text-muted mb-0" style="font-size: large; font-weight: bold">{{ translate("Don't have an account?")}}</p>
                                    <a href="{{ route('user.registration') }}" style="font-size: large; font-weight: bold">{{ translate('Register Now')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript">
        function autoFillSeller(){
            $('#email').val('seller@example.com');
            $('#password').val('123456');
        }
        function autoFillCustomer(){
            $('#email').val('customer@example.com');
            $('#password').val('123456');
        }
    </script>


<script>
/*Login section ends..........*/

// $(document).ready(function(){
//     //check phone number length
//     $("#phone_number").on('input',function () {
        
//       var number = $('.phone_number').val();
//       if (number.length != 14) {
//         $(".submit-btn").attr('disabled','true');
//         $(".submit-btn").attr('style','background-color: gray');
//         $("#otp").attr('style', 'display: none');
//       }else {
//         $(".submit-btn").removeAttr('disabled');
//         $(".submit-btn").removeAttr('style');
//       }

//       Open otp form after click submit
//       $(".submit-btn").click(function () {
//           if ( $('.submit-btn').attr('onclick') == 'return false' ) {
//               submitNumber();
//               $("#otp").removeAttr('style');
//               $(".submit-btn").removeAttr('onclick');
//           }
//       })

//        Send otp to number
//       function submitNumber() {
//         var phone = $('.phone_number').val();
//         $.ajax({
//           type: "get",
//           url: "{{route('user.otp.send')}}",
//           data: { number: phone },
//           success: function(msg) {
//               if (msg == "success"){
//                   //Show dismissible success message
//                   $('#success_msg').removeAttr('style');
//               }else if(msg == "unRegister"){
//                  window.location = "registration";
//               }else{
//                   $('#error_msg').removeAttr('style');
//               }
//           }
//         })
//       }
//     })

// });
/*Login section ends..........*/
</script>

@endsection
