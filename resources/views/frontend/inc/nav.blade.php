
<header class="@if(get_setting('header_stikcy') == 'on') sticky-top @endif z-1020 bg-white border-bottom shadow-sm">
    <div class="position-relative logo-bar-area z-1">
        <div class="container">
            <div class="d-flex align-items-center">

                <div class="col-auto col-xl-2 pl-0 pr-3 d-flex align-items-center">
                    <a class="d-block pt-0px pb-15px mr-3 ml-0" href="{{ route('home') }}">
                        @php
                            $header_logo = get_setting('header_logo');
                        @endphp
                        @if($header_logo != null)
                            <img src="{{ uploaded_asset($header_logo) }}" alt="{{ env('APP_NAME') }}" class="w-180px" >
                        @else
                            <img src="{{ static_asset('assets/img/logo.png') }}" alt="{{ env('APP_NAME') }}" class="mw-100 h-30px h-md-40px" height="40">
                        @endif
                    </a>

                        
                </div>
                <div class="d-lg-none ml-auto mr-0">
                    <a class="p-2 d-block text-reset" href="javascript:void(0);" data-toggle="class-toggle" data-target=".front-header-search">
                        <i class="las la-search la-flip-horizontal la-2x"></i>
                    </a>
                </div>

                <div class="flex-grow-1 front-header-search pl-20px pr-20px d-flex align-items-center bg-white">
                    <div class="position-relative flex-grow-1">
                        <form action="{{ route('search') }}" method="GET" class="stop-propagation">
                            <div class="d-flex position-relative align-items-center">
                                <div class="d-lg-none" data-toggle="class-toggle" data-target=".front-header-search">
                                    <button class="btn px-2" type="button"><i class="la la-2x la-long-arrow-left"></i></button>
                                </div>
                                <div class="input-group">
                                    <input type="text" class="border-0 border-lg form-control" id="search" name="q" placeholder="{{translate('I am shopping for...')}}" autocomplete="off">
                                    <div class="input-group-append d-none d-lg-block">
                                        <button class="btn btn-primary" type="submit">
                                            <i class="la la-search la-flip-horizontal fs-18"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="typed-search-box stop-propagation document-click-d-none d-none bg-white rounded shadow-lg position-absolute left-0 top-100 w-100" style="min-height: 200px">
                            <div class="search-preloader absolute-top-center">
                                <div class="dot-loader"><div></div><div></div><div></div></div>
                            </div>
                            <div class="search-nothing d-none p-3 text-center fs-16">

                            </div>
                            <div id="search-content" class="text-left">

                            </div>
                        </div>
                    </div>
                </div>

                <div class="d-none d-lg-none ml-3 mr-0">
                    <div class="nav-search-box">
                        <a href="#" class="nav-box-link">
                            <i class="la la-search la-flip-horizontal d-inline-block nav-box-icon"></i>
                        </a>
                    </div>
                </div>

                <div class="d-none d-lg-block ml-3 mr-0 hcs">
                    
                    <i class="las la-phone-volume la-2x"></i>
                    
                    <div class="text-s">
                        <span>Contact</span>
                        <p>+8801755700177</p>
                    </div>

                </div>

                
            </div>
        </div>
    </div>
    @if ( get_setting('header_menu_labels') !=  null )
        <div class="bg-white border-top border-gray-200 py-2">
            <div class="container">
                <div class="d-flex align-items-center">

                    <!-- <div class="d-none d-lg-block mr-0 dpcat">
                        
                          
                            <div class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="text-s">
                                   <i class="las la-bars la-2x"></i> 
                                   <span>Shop By Categories</span>
                                </div>
                            </div>

                             <div class="hover-category-menu position-absolute w-100 top-0 left-0 right-0 dnone z-3 dropdown-menu" aria-labelledby="dropdownMenuButton" style="background:none; border: 0;-webkit-box-shadow: none; box-shadow: none;">
                                <div class="container">
                                    <div class="row gutters-10 position-relative">
                                        <div class="col-lg-3 position-static">
                                            @include('frontend.partials.category_menu')
                                        </div>
                                    </div>
                                </div>
                            </div>

                    </div> -->

                    <div class="flex-grow-1 d-flex">

                        <!-- <ul class="list-inline mb-0 pl-0 mobile-hor-swipe text-center w-full">
                            @foreach (json_decode( get_setting('header_menu_labels'), true) as $key => $value)
                            <li class="list-inline-item mr-0">
                                <a href="{{ json_decode( get_setting('header_menu_links'), true)[$key] }}" class="fs-14 px-3 py-2 d-inline-block fw-600 hov-opacity-100 text-reset">
                                    {{ translate($value) }}
                                </a>
                            </li>
                            @endforeach
                        </ul> -->
                        
                        <?php
                            $mainMenu = \App\Category::where('level', 0)->where('parent_id',0)->orderBy('order_level', 'desc')->get()->take(8);

                            $mainMenuId= array();
                            foreach($mainMenu as $manu){
                                array_push($mainMenuId, $manu->id);
                            }
                            $subMenu = \App\Category::whereIn('parent_id', $mainMenuId)->get();

                            $subMenuId= array();
                            foreach($subMenu as $smanu){
                                array_push($subMenuId, $smanu->id);
                            }
                            $childMenu = \App\Category::whereIn('parent_id', $subMenuId)->get();
                       ?>

                        <div class="stellarnav">
                            <ul>
                                @foreach($mainMenu as $key => $value)
                                    <li ><a  href="{{ route('products.category', $value->slug) }}">{{ $value->name }}</a>
                                        <?php
                                            $countSub = \App\Category::where('parent_id', $value->id)->count();
                                        ?>
                                        @if($countSub > 0)
                                            <ul>
                                                @foreach($subMenu as $sub)
                                                    @if($value->id == $sub->parent_id)
                                                        <li><a  href="{{ route('products.category', $sub->slug) }}">{{ $sub->name }}</a>
                                                            <?php
                                                                $childCount = \App\Category::where('parent_id', $sub->id)->count();
                                                            ?>
                                                            @if($childCount > 0)
                                                                <ul>
                                                                    @foreach($childMenu as $child)
                                                                        @if($sub->id == $child->parent_id)
                                                                        <li><a href="{{ route('products.category', $child->slug) }}">{{ $child->name }}</a></li>
                                                                        @endif
                                                                    @endforeach
                                                                </ul>
                                                            @endif
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                 @endforeach
                            </ul>
                        </div>


                    </div>
                    

                    <div class="d-none d-lg-block ml-4 mr-0 hls">
                        @auth
                        <div class="dropdown">
                          
                            <div class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="text-s">
                                    <span>Sign in</span>
                                    <p class="mb-0">My Account</p>
                                </div>
                            </div>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                @if(isAdmin())
                                <a class="dropdown-item" href="{{ route('admin.dashboard') }}">{{ translate('My Panel')}}</a>
                                @else
                                <a class="dropdown-item" href="{{ route('dashboard') }}">{{ translate('My Panel')}}</a>
                                @endif
                                <a class="dropdown-item" href="{{ route('logout') }}">{{ translate('Logout')}}</a>
                            </div>

                        </div>

                    @else
                        <div class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="text-s">
                                <span>Sign in</span>
                                <p class="mb-0">My Account</p>
                            </div>
                        </div>

                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="{{ route('user.login') }}">{{ translate('Login')}}</a>

                            <a class="dropdown-item" href="{{ route('user.registration') }}">{{ translate('Registration')}}</a>
                        </div>
                    @endauth

                </div>



                    <div class="d-none d-lg-block  align-self-stretch ml-3 mr-0" data-hover="dropdown">
                        <div class="nav-cart-box dropdown h-100" id="cart_items">
                            @include('frontend.partials.cart')
                        </div>
                    </div>



                 </div>

            </div>

        </div>
    @endif
</header>
