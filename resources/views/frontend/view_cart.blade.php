@extends('frontend.layouts.app')

@section('content')

<section class="pt-5 mb-4">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 mx-auto">
                <div class="row aiz-steps arrow-divider">
                    <div class="col active">
                        <div class="text-center text-primary">
                            <i class="la-3x mb-2 las la-shopping-cart"></i>
                            <h3 class="fs-14 fw-600 d-none d-lg-block">{{ translate('1. My Cart')}}</h3>
                        </div>
                    </div>
                    <div class="col">
                        <div class="text-center">
                            <i class="la-3x mb-2 opacity-50 las la-map"></i>
                            <h3 class="fs-14 fw-600 d-none d-lg-block opacity-50">{{ translate('2. Shipping info')}}</h3>
                        </div>
                    </div>
                    <div class="col">
                        <div class="text-center">
                            <i class="la-3x mb-2 opacity-50 las la-truck"></i>
                            <h3 class="fs-14 fw-600 d-none d-lg-block opacity-50">{{ translate('3. Delivery info')}}</h3>
                        </div>
                    </div>
                    <div class="col">
                        <div class="text-center">
                            <i class="la-3x mb-2 opacity-50 las la-credit-card"></i>
                            <h3 class="fs-14 fw-600 d-none d-lg-block opacity-50">{{ translate('4. Payment')}}</h3>
                        </div>
                    </div>
                    <div class="col">
                        <div class="text-center">
                            <i class="la-3x mb-2 opacity-50 las la-check-circle"></i>
                            <h3 class="fs-14 fw-600 d-none d-lg-block opacity-50">{{ translate('5. Confirmation')}}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mb-4" id="cart-summary">
    <div class="container">
        {{-- @if( Session::has('cart') && count(Session::get('cart')) > 0 ) --}}
        @if( $carts && count($carts) > 0 )
            <div class="row">
                <div class="col-xxl-8 col-xl-10 mx-auto">
                    <div class="shadow-sm bg-white p-3 p-lg-4 rounded text-left">
                        <div class="mb-4">
                            <div class="row gutters-5 d-none d-lg-flex border-bottom mb-3 pb-3">
                                <div class="col-md-5 fw-600">{{ translate('Product')}}</div>
                                <div class="col fw-600">{{ translate('Price')}}</div>
                                <div class="col fw-600">{{ translate('Tax')}}</div>
                                <div class="col fw-600">{{ translate('Quantity')}}</div>
                                <div class="col fw-600">{{ translate('Total')}}</div>
                                <div class="col-auto fw-600">{{ translate('Remove')}}</div>
                            </div>
                            <ul class="list-group list-group-flush">
                                @php
                                $total = 0;
                                @endphp
                                @foreach ($carts as $key => $cartItem)
                                    @php
                                    $product = \App\Product::find($cartItem['product_id']);
                                    $total = $total + $cartItem['price']*$cartItem['quantity'];
                                    $product_name_with_choice = $product->getTranslation('name');
                                    if ($cartItem['variation'] != null) {
                                        $product_name_with_choice = $product->getTranslation('name').' - '.$cartItem['variation'];
                                    }
                                    @endphp
                                    @if($cartItem['variation'] != null)
                                        @foreach($product->stocks as $stock)
                                            @if($stock->variant == $cartItem['variation'])
                                                @php
                                                    $image = $stock['image'];
                                                @endphp
                                            @endif
                                        @endforeach
                                    @else
                                        @php
                                            $image = $product->thumbnail_img;
                                        @endphp
                                    @endif

                                    <li class="list-group-item px-0 px-lg-3">
                                        <div class="row gutters-5">
                                            <div class="col-lg-5 d-flex">
                                                <span class="mr-2 ml-0">
                                                    <img
                                                        src="{{ uploaded_asset($image) }}"
                                                        class="img-fit size-60px rounded"
                                                        alt="{{  $product->getTranslation('name')  }}"
                                                    >
                                                </span>
                                                <span class="fs-14 opacity-60">{{ $product_name_with_choice }}</span>
                                            </div>

                                            <div class="col-lg col-4 order-1 order-lg-0 my-3 my-lg-0">
                                                <span class="opacity-60 fs-12 d-block d-lg-none">{{ translate('Price')}}</span>
                                                <span class="fw-600 fs-16">{{ single_price($cartItem['price']) }}</span>
                                            </div>
                                            <div class="col-lg col-4 order-2 order-lg-0 my-3 my-lg-0">
                                                <span class="opacity-60 fs-12 d-block d-lg-none">{{ translate('Tax')}}</span>
                                                <span class="fw-600 fs-16">{{ single_price($cartItem['tax']) }}</span>
                                            </div>

                                            <div class="col-lg col-6 order-4 order-lg-0">
                                                @if($cartItem['digital'] != 1)
                                                    <div class="row no-gutters align-items-center aiz-plus-minus mr-2 ml-0">
                                                        <button class="btn col-auto btn-icon btn-sm btn-circle btn-light" type="button" data-type="minus" data-field="quantity[{{ $cartItem['id'] }}]">
                                                            <i class="las la-minus"></i>
                                                        </button>
                                                        <input type="text" name="quantity[{{ $cartItem['id'] }}]" class="col border-0 text-center flex-grow-1 fs-16 input-number" placeholder="1" value="{{ $cartItem['quantity'] }}" min="1" max="10" readonly onchange="updateQuantity({{ $cartItem['id'] }}, this)">
                                                        <button class="btn col-auto btn-icon btn-sm btn-circle btn-light" type="button" data-type="plus" data-field="quantity[{{ $cartItem['id'] }}]">
                                                            <i class="las la-plus"></i>
                                                        </button>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-lg col-4 order-3 order-lg-0 my-3 my-lg-0">
                                                <span class="opacity-60 fs-12 d-block d-lg-none">{{ translate('Total')}}</span>
                                                <span class="fw-600 fs-16 text-primary">{{ single_price(($cartItem['price'] + $cartItem['tax']) * $cartItem['quantity']) }}</span>
                                            </div>
                                            <div class="col-lg-auto col-6 order-5 order-lg-0 text-right">
                                                <a href="javascript:void(0)" onclick="removeFromCartView(event, {{ $cartItem['id'] }})" class="btn btn-icon btn-sm btn-soft-primary btn-circle">
                                                    <i class="las la-trash"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach

                                @php
                                    $special_offer = \App\SpecialOffer::where('status', 1)->latest()->first();
                                    $date = strtotime(date('d-m-Y'));
                                @endphp

                                @if(isset($special_offer) && $special_offer != null)
                                    @if ($special_offer->offer_type == 'product_base')
                                    @if ($special_offer->discount_method == 'buy_get_offer' && $date >= $special_offer->start_date && $date <= $special_offer->end_date)
                                        @foreach ($carts as $key => $cartItem)
                                            @php
                                                $product_id_belong_offer = json_decode($special_offer->product_ids);
                                                $product_id_with_offer = $special_offer->offer_product_ids;
                                                $product_id = $cartItem['product_id'];

                                            @endphp
                                            @if(in_array($product_id,$product_id_belong_offer))
                                                @php
                                                    $product = \App\Product::find($product_id_with_offer);
                                                    $image_thum = $product->thumbnail_img;
                                                    $cartItem['price'] = 0;
                                                    $cartItem['tax'] = 0;
                                                @endphp
                                            <li class="list-group-item px-0 px-lg-3">
                                                <div class="row gutters-5">
                                                    <div class="col-lg-5 d-flex">
                                                        <span class="mr-2 ml-0">
                                                            <img
                                                                src="{{ uploaded_asset($image_thum) }}"
                                                                class="img-fit size-60px rounded"
                                                                alt="{{  $product->getTranslation('name')  }}"
                                                            >
                                                        </span>
                                                        <span class="fs-14 opacity-60">{{ $product->name }}</span>
                                                    </div>

                                                    <div class="col-lg col-4 order-1 order-lg-0 my-3 my-lg-0">
                                                        <span class="opacity-60 fs-12 d-block d-lg-none">{{ translate('Price')}}</span>
                                                        <span class="fw-600 fs-16">{{ single_price($cartItem['price']) }}</span>
                                                    </div>
                                                    <div class="col-lg col-4 order-2 order-lg-0 my-3 my-lg-0">
                                                        <span class="opacity-60 fs-12 d-block d-lg-none">{{ translate('Tax')}}</span>
                                                        <span class="fw-600 fs-16">{{ single_price($cartItem['tax']) }}</span>
                                                    </div>

                                                    <div class="col-lg col-6 order-4 order-lg-0">
                                                        @if($cartItem['digital'] != 1)
                                                            <div class="row no-gutters align-items-center aiz-plus-minus mr-2 ml-0">
                                                                <button class="btn col-auto btn-icon btn-sm btn-circle btn-light" style="disabled" type="button" data-type="minus">
                                                                    <i class="las la-minus"></i>
                                                                </button>
                                                                <input type="text" name="quantity[{{ $cartItem['id'] }}]" class="col border-0 text-center flex-grow-1 fs-16 input-number" placeholder="1" value="1" min="1" max="10" readonly>
                                                                <button class="btn col-auto btn-icon btn-sm btn-circle btn-light" style="disabled" type="button" data-type="plus">
                                                                    <i class="las la-plus"></i>
                                                                </button>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="col-lg col-4 order-3 order-lg-0 my-3 my-lg-0">
                                                        <span class="opacity-60 fs-12 d-block d-lg-none">{{ translate('Total')}}</span>
                                                        <span class="fw-600 fs-16 text-primary">{{ single_price(($cartItem['price'] + $cartItem['tax']) * $cartItem['quantity']) }}</span>
                                                    </div>
                                                    <div class="col-lg-auto col-6 order-5 order-lg-0 text-right">
                                                        <a href="javascript:void(0)" class="btn btn-icon btn-sm btn-soft-primary btn-circle" style="disabled">
                                                            <i class="las la-trash"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            @endif
                                        @endforeach
                                    @endif
                                @endif
                                @endif
                            </ul>
                        </div>

                        @php
                            $special_offer = \App\SpecialOffer::where('status', 1)->latest()->first();
                            $date = strtotime(date('d-m-Y'));
                            $total_quantity = 0;
                            $free_discount = 0;


                            if (isset($special_offer) && $special_offer != null && $date >= $special_offer->start_date && $date <= $special_offer->end_date) {
                                if($special_offer->offer_type == 'cart_base'){
                                    if($special_offer->discount_method == 'flat_discount_offer'){
                                        if($special_offer != null){
                                            if($total >= $special_offer->min_buy && $date >= $special_offer->start_date && $date <= $special_offer->end_date){
                                                if($special_offer->discount_type == 'amount') {
                                                    $total -= $special_offer->discount;
                                                }else if ($special_offer->discount_type == 'percent') {
                                                    $discount = ($total * $special_offer->discount) / 100;
                                                    if ($discount > $special_offer->max_discount) {
                                                        $discount = $special_offer->max_discount;
                                                    }
                                                    $total -= $discount;
                                                }
                                            }
                                        }
                                    }else if($special_offer->discount_method == 'quantity_offer'){
                                        if($date >= $special_offer->start_date && $date <= $special_offer->end_date){
                                            foreach ($carts as $key => $cartItem){
                                                $total_quantity += $cartItem['quantity'];
                                                if($total_quantity >= $special_offer->min_qty){
                                                    $free_discount = $cartItem['price']*$special_offer->discount_qty;
                                                }
                                            }
                                            $total -= $free_discount;
                                        }
                                    }if($special_offer->discount_method == 'discount_on_quantiry'){
                                        $total = 0;
                                        if($date >= $special_offer->start_date && $date <= $special_offer->end_date){
                                            foreach ($carts as $key => $cartItem){
                                                $total_quantity += $cartItem['quantity'];
                                                $total = $total + $cartItem['price']*$cartItem['quantity'];
                                                if($total_quantity >= $special_offer->min_qty){

                                                    if($special_offer->discount_type == 'amount') {
                                                    $total -= $special_offer->discount;

                                                    }else if ($special_offer->discount_type == 'percent') {
                                                        $discount = ($total * $special_offer->discount) / 100;
                                                        if ($discount > $special_offer->max_discount) {
                                                            $discount = $special_offer->max_discount;
                                                        }
                                                        $total -= $discount;
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }else if($special_offer->offer_type == 'product_base'){
                                    if($special_offer->discount_method == 'price_break_discount'){
                                        $total = 0;
                                        $offered_product_id = json_decode($special_offer->product_ids);
                                        $offered_product_qty = json_decode($special_offer->min_qty_Price_break);
                                        $offered_product_price = json_decode($special_offer->discount_Price_break);

                                        $quantity_count = count($offered_product_price);
                                        $discount = 0;
                                        $offer_qty_exist = 0;$discount_offer = 0;

                                        foreach ($carts as $key => $cartItem){
                                            $item_quantity = $cartItem['quantity'];
                                            $product_id = $cartItem['product_id'];
                                            $total = $total + $cartItem['price']*$cartItem['quantity'];

                                            if(in_array($product_id,$offered_product_id)){
                                                $offer_qty_exist += $cartItem['quantity'];
                                            }
                                        }
                                        foreach($offered_product_qty as $key=>$offered_qty){
                                            if($offered_qty == $offer_qty_exist){
                                                $discount_offer = $offered_product_price[$key];
                                            }else if($offer_qty_exist > $offered_product_qty[$key]){
                                                $discount_offer = 0;
                                                $discount_offer = $offered_product_price[$key];
                                            }
                                        }

                                        $total -= $discount_offer;
                                    }else if($special_offer->discount_method == 'flat_discount_offer'){
                                        $total = 0;
                                        $offer_amount = 0;$discount = 0;
                                        $offered_product_id = json_decode($special_offer->product_ids);

                                        foreach ($carts as $key => $cartItem){
                                            $item_quantity = $cartItem['quantity'];
                                            $product_id = $cartItem['product_id'];
                                            $total = $total + $cartItem['price']*$cartItem['quantity'];

                                            if(in_array($product_id,$offered_product_id)){
                                                $offer_amount += $cartItem['price']*$cartItem['quantity'];
                                            }
                                        }

                                        if($offer_amount >= $special_offer->min_buy && $date >= $special_offer->start_date && $date <= $special_offer->end_date){
                                            if($special_offer->discount_type == 'amount') {
                                                $total -= $special_offer->discount;
                                            }else if ($special_offer->discount_type == 'percent') {
                                                $discount = ($offer_amount * $special_offer->discount) / 100;
                                                if ($discount > $special_offer->max_discount) {
                                                    $discount = $special_offer->max_discount;
                                                }
                                                $total -= $discount;
                                            }
                                        }
                                    }else if($special_offer->discount_method == 'discount_on_quantiry'){
                                        $total = 0;
                                        $offer_amount = 0;$discount = 0;$offer_quantity=0;
                                        $offered_product_id = json_decode($special_offer->product_ids);

                                        foreach ($carts as $key => $cartItem){
                                            $item_quantity = $cartItem['quantity'];
                                            $product_id = $cartItem['product_id'];
                                            $total = $total + $cartItem['price']*$cartItem['quantity'];

                                            if(in_array($product_id,$offered_product_id)){
                                                $offer_amount += $cartItem['price']*$cartItem['quantity'];
                                                $offer_quantity += $cartItem['quantity'];
                                            }
                                        }

                                        if($offer_quantity >= $special_offer->min_qty && $date >= $special_offer->start_date && $date <= $special_offer->end_date){
                                            if($special_offer->discount_type == 'amount') {
                                                $total -= $special_offer->discount;
                                            }else if ($special_offer->discount_type == 'percent') {
                                                $discount = ($offer_amount * $special_offer->discount) / 100;
                                                if ($discount > $special_offer->max_discount) {
                                                    $discount = $special_offer->max_discount;
                                                }
                                                $total -= $discount;
                                            }
                                        }

                                    }else if($special_offer->discount_method == 'quantity_offer'){
                                        $total = 0;
                                        $discount = 0;$offer_quantity=0;
                                        $offered_product_id = json_decode($special_offer->product_ids);

                                        foreach ($carts as $key => $cartItem){
                                            $product_id = $cartItem['product_id'];
                                            $total = $total + $cartItem['price']*$cartItem['quantity'];
                                            if(in_array($product_id,$offered_product_id)){
                                                $offer_quantity += $cartItem['quantity'];
                                            }
                                        }
                                        if($offer_quantity >= $special_offer->min_qty){
                                            $discount = $cartItem['price']*$special_offer->discount_qty;
                                        }
                                        $total -= $discount;

                                    }
                                }
                            }

                        @endphp
                        <div class="px-3 py-2 mb-4 border-top d-flex justify-content-between">
                            <span class="opacity-60 fs-15">{{translate('Subtotal')}}</span>
                            <span class="fw-600 fs-17">{{ single_price($total) }}</span>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-md-6 text-center text-md-left order-1 order-md-0">
                                <a href="{{ route('home') }}" class="btn btn-link">
                                    <i class="las la-arrow-left"></i>
                                    {{ translate('Return to shop')}}
                                </a>
                            </div>
                            <div class="col-md-6 text-center text-md-right">
                                @if(Auth::check())
                                    <a href="{{ route('checkout.shipping_info') }}" class="btn btn-primary fw-600">
                                        {{ translate('Continue to Shipping')}}
                                    </a>
                                @else
                                    <button class="btn btn-primary fw-600" onclick="showCheckoutModal()">{{ translate('Continue to Shipping')}}</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-xl-8 mx-auto">
                    <div class="shadow-sm bg-white p-4 rounded">
                        <div class="text-center p-3">
                            <i class="las la-frown la-3x opacity-60 mb-3"></i>
                            <h3 class="h4 fw-700">{{translate('Your Cart is empty')}}</h3>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</section>

@endsection

@section('modal')
    <div class="modal fade" id="GuestCheckout">
        <div class="modal-dialog modal-dialog-zoom">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title fw-600">{{ translate('Login')}}</h6>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true"></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="p-3">
                        {{--Success message--}}
                        <p class="alert alert-success alert-dismissible fade show" id="success_msg" style="display: none">
                            <strong>Confirmation code has been sent to your phone.</strong>
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                        </p>
                        {{--Error message--}}
                        <p class="alert alert-danger alert-dismissible fade show" id="error_msg" style="display: none">
                            <strong>Error!</strong> Something went wrong,Try again.
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                        </p>
                        {{-- <form class="form-default" role="form" action="{{ route('user.otp.login') }}" method="POST"> --}}

                            {{-- Login without otp --}}
                            <form class="form-default" role="form" action="{{ route('user.otp.send') }}" method="POST">

                            @csrf
                            <!-- <div class="form-group">
                                @if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated)
                                    <input type="text" class="form-control h-auto form-control-lg {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="{{ translate('Email Or Phone')}}" name="email" id="email">
                                @else
                                    <input type="email" class="form-control h-auto form-control-lg {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="{{  translate('Email') }}" name="email">
                                @endif
                                @if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated)
                                    <span class="opacity-60">{{  translate('Use country code before number') }}</span>
                                @endif
                            </div> -->
                            <input type="hidden" name="cart" value="1">
                            <div class="form-group">
                                @if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated)
                                    <input type="text" class="form-control phone_number {{ $errors->has('phone') ? ' is-invalid' : '' }}" value="{{ old('phone', '+8801') }}" placeholder="{{ translate('Phone')}}" name="phone" id="phone_number">
                                @else
                                    <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="{{  translate('Email') }}" name="email">
                                @endif
                                @if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated)
                                    <span class="opacity-60">{{  translate('Use country code before number') }}</span>
                                @endif
                            </div>
                            {{-- <div class="form-group" id="otp" style="display: none">
                                <input type="number" class="form-control" name="otp" placeholder="Enter confirmation code" required>
                                <i class="icofont-pin"></i>
                            </div> --}}

                            <div class="row mb-2">
                                <div class="col-6">
                                    <label class="aiz-checkbox">
                                        <input type="checkbox" name="remember" checked>
                                        <span class=opacity-60>{{  translate('Remember Me') }}</span>
                                        <span class="aiz-square-check"></span>
                                    </label>
                                </div>
                                <!-- <div class="col-6 text-right">
                                    <a href="{{ route('password.request') }}" class="text-reset opacity-60 fs-14">{{ translate('Forgot password?')}}</a>
                                </div> -->
                            </div>

                            <div class="mb-5">
                                <button type="submit" class="btn btn-primary btn-block submit-btn fw-600" style="background-color: #f11005f0">{{  translate('Login') }}</button>
                                {{-- <button type="submit" onclick="return false" class="btn btn-primary btn-block submit-btn fw-600" style="background-color: gray" disabled>{{  translate('Login') }}</button> --}}
                            </div>
                        </form>

                    </div>
                    <div class="text-center mb-3">
                        <p class="text-muted mb-0" style="font-size: large; font-weight: bold">{{ translate("Don't have an account?")}}</p>
                        <a href="{{ route('user.registration', ['reg'=>1]) }}" style="font-size: large; font-weight: bold">{{ translate('Register Now')}}</a>
                    </div>
                    @if(get_setting('google_login') == 1 || get_setting('facebook_login') == 1 || get_setting('twitter_login') == 1)
                        <div class="separator mb-3">
                            <span class="bg-white px-3 opacity-60">{{ translate('Or Login With')}}</span>
                        </div>
                        <ul class="list-inline social colored text-center mb-3">
                            @if (get_setting('facebook_login') == 1)
                                <li class="list-inline-item">
                                    <a href="{{ route('social.login', ['provider' => 'facebook']) }}" class="facebook">
                                        <i class="lab la-facebook-f"></i>
                                    </a>
                                </li>
                            @endif
                            @if(get_setting('google_login') == 1)
                                <li class="list-inline-item">
                                    <a href="{{ route('social.login', ['provider' => 'google']) }}" class="google">
                                        <i class="lab la-google"></i>
                                    </a>
                                </li>
                            @endif
                            @if (get_setting('twitter_login') == 1)
                                <li class="list-inline-item">
                                    <a href="{{ route('social.login', ['provider' => 'twitter']) }}" class="twitter">
                                        <i class="lab la-twitter"></i>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        function removeFromCartView(e, key){
            e.preventDefault();
            removeFromCart(key);
        }

        function updateQuantity(key, element){
            $.post('{{ route('cart.updateQuantity') }}', {
                _token   :  '{{ csrf_token() }}',
                id       :  key,
                quantity :  element.value
            }, function(data){
                updateNavCart();
                $('#cart-summary').html(data);
            });
        }

        function showCheckoutModal(){
            $('#GuestCheckout').modal();
        }
    </script>

{{-- <script>
/*Login section ends..........*/

$(document).ready(function(){
    //check phone number length
    $("#phone_number").on('input',function () {

      var number = $('.phone_number').val();
      if (number.length != 14) {
        $(".submit-btn").attr('disabled','true');
        $(".submit-btn").attr('style','background-color: gray');
        $("#otp").attr('style', 'display: none');
      }else {
        $(".submit-btn").removeAttr('disabled');
        $(".submit-btn").removeAttr('style');
      }

      //Open otp form after click submit
      $(".submit-btn").click(function () {
          if ( $('.submit-btn').attr('onclick') == 'return false' ) {
              submitNumber();
              $("#otp").removeAttr('style');
              $(".submit-btn").removeAttr('onclick');
          }
      })

      //Send otp to number
      function submitNumber() {
        var phone = $('.phone_number').val();
        $.ajax({
          type: "get",
          url: "{{route('user.otp.send')}}",
          data: { number: phone },
          success: function(msg) {
              if (msg == "success"){
                  //Show dismissible success message
                  $('#success_msg').removeAttr('style');
              }else if(msg == "unRegister"){
                 window.location = "users/registration?reg=1";
              }else{
                  $('#error_msg').removeAttr('style');
              }
          }
        })
      }
    })

});
/*Login section ends..........*/
</script> --}}
@endsection
