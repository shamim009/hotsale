<div class="card border-0 shadow-sm rounded">
    <div class="card-header">
        <h3 class="fs-16 fw-600 mb-0">{{translate('Summary')}}</h3>
        <div class="text-right">
            <span class="badge badge-inline badge-primary">
                {{ count($carts) }}
                {{translate('Items')}}
            </span>
        </div>
    </div>

    <div class="card-body">
        @if (\App\Addon::where('unique_identifier', 'club_point')->first() != null && \App\Addon::where('unique_identifier', 'club_point')->first()->activated)
            @php
                $total_point = 0;
            @endphp
            @foreach ($carts as $key => $cartItem)
                @php
                    $product = \App\Product::find($cartItem['product_id']);
                    $total_point += $product->earn_point * $cartItem['quantity'];
                @endphp
            @endforeach

            <div class="rounded px-2 mb-2 bg-soft-primary border-soft-primary border">
                {{ translate("Total Club point") }}:
                <span class="fw-700 float-right">{{ $total_point }}</span>
            </div>
        @endif
        <table class="table">
            <thead>
                <tr>
                    <th class="product-name">{{translate('Product')}}</th>
                    <th class="product-total text-right">{{translate('Total')}}</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $subtotal = 0;
                    $tax = 0;
                    $shipping = 0;
                    $total_quantity = 0;
                    $product_shipping_cost = 0;
                    $shipping_region = $shipping_info['city'];
                @endphp
                @foreach ($carts as $key => $cartItem)
                    @php
                        $product = \App\Product::find($cartItem['product_id']);
                        $subtotal += $cartItem['price'] * $cartItem['quantity'];
                        $tax += $cartItem['tax'] * $cartItem['quantity'];
                        $total_quantity += $cartItem['quantity'];

                        $product_shipping_cost = $cartItem['shipping_cost'];

                        if($product->is_quantity_multiplied == 1 && get_setting('shipping_type') == 'product_wise_shipping') {
                            $product_shipping_cost = $product_shipping_cost * $cartItem['quantity'];
                        }

                        $shipping += $product_shipping_cost;

                        $product_name_with_choice = $product->getTranslation('name');
                        if ($cartItem['variant'] != null) {
                            $product_name_with_choice = $product->getTranslation('name').' - '.$cartItem['variant'];
                        }
                    @endphp
                    <tr class="cart_item">
                        <td class="product-name">
                            {{ $product_name_with_choice }}
                            <strong class="product-quantity">
                                × {{ $cartItem['quantity'] }}
                            </strong>
                        </td>
                        <td class="product-total text-right">
                            <span class="pl-4 pr-0">{{ single_price($cartItem['price']*$cartItem['quantity']) }}</span>
                        </td>
                    </tr>
                @endforeach
                @php
                    $special_offer = \App\SpecialOffer::where('status', 1)->latest()->first();
                    $date = strtotime(date('d-m-Y'));
                @endphp

                @if(isset($special_offer) && $special_offer != null && $date >= $special_offer->start_date && $date <= $special_offer->end_date)
                    @if ($special_offer->offer_type == 'product_base')
                    @if ($special_offer->discount_method == 'buy_get_offer' && $date >= $special_offer->start_date && $date <= $special_offer->end_date)
                        @foreach ($carts as $key => $cartItem)
                            @php
                                $product_id_belong_offer = json_decode($special_offer->product_ids);
                                $product_id_with_offer = $special_offer->offer_product_ids;
                                $product_id = $cartItem['product_id'];

                            @endphp
                            @if(in_array($product_id,$product_id_belong_offer))
                                @php
                                    $product = \App\Product::find($product_id_with_offer);
                                    $cartItem['price'] = 0;
                                    $cartItem['quantity'] = 1;
                                @endphp
                                <tr class="cart_item">
                                    <td class="product-name">
                                        {{ $product->name }}
                                        <strong class="product-quantity">
                                            × {{ $cartItem['quantity'] }}[Free]
                                        </strong>
                                    </td>
                                    <td class="product-total text-right">
                                        <span class="pl-4 pr-0">{{ single_price($cartItem['price']*$cartItem['quantity']) }}</span>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    @endif
                @endif
                @endif


            </tbody>
        </table>

        <table class="table">

            <tfoot>
                <tr class="cart-subtotal">
                    <th>{{translate('Subtotal')}}</th>
                    <td class="text-right">
                        <span class="fw-600">{{ single_price($subtotal) }}</span>
                    </td>
                </tr>

                <tr class="cart-shipping">
                    <th>{{translate('Tax')}}</th>
                    <td class="text-right">
                        <span class="font-italic">{{ single_price($tax) }}</span>
                    </td>
                </tr>
                @if(isset($special_offer) && $special_offer != null && $date >= $special_offer->start_date && $date <= $special_offer->end_date)
                        @if($special_offer->offer_type == 'cart_base')
                        @if($special_offer->discount_method == 'delivery_chare_on_amount')
                            @if ($total >= $special_offer->min_buy && $date >= $special_offer->start_date && $date <= $special_offer->end_date)
                                @php
                                    $shipping = 0;
                                @endphp
                            @endif
                        @elseif ($special_offer->discount_method == 'delivery_chare_on_quantity')
                            @if($total_quantity >= $special_offer->min_qty && $date >= $special_offer->start_date && $date <= $special_offer->end_date)
                                @php
                                    $shipping = 0;
                                @endphp
                            @endif
                        @elseif ($special_offer->discount_method == 'flat_discount_offer')
                            @if($special_offer != null)
                                @if($total >= $special_offer->min_buy && $date >= $special_offer->start_date && $date <= $special_offer->end_date)
                                    @if($special_offer->discount_type == 'amount')
                                    @php
                                        $discount = $special_offer->discount;
                                        $total -= $discount;
                                    @endphp
                                    @elseif ($special_offer->discount_type == 'percent')
                                    @php
                                        $discount = ($total * $special_offer->discount) / 100;
                                        if ($discount > $special_offer->max_discount) {
                                            $discount = $special_offer->max_discount;
                                        }
                                        $total -= $discount;
                                    @endphp
                                    @endif
                                @endif
                            @endif
                        @elseif($special_offer->discount_method == 'quantity_offer')
                            @php
                                $discount = 0;
                                if($date >= $special_offer->start_date && $date <= $special_offer->end_date){
                                        foreach ($carts as $key => $cartItem){
                                            $total_quantity += $cartItem['quantity'];
                                            if($total_quantity >= $special_offer->min_qty){
                                                $discount = $cartItem['price'];
                                            }
                                        }
                                        $total -= $discount;
                                    }
                            @endphp
                        @elseif($special_offer->discount_method == 'discount_on_quantiry')
                           @php
                               $total = 0;$discount = 0;
                               if($date >= $special_offer->start_date && $date <= $special_offer->end_date){
                                    foreach ($carts as $key => $cartItem){
                                        $total_quantity += $cartItem['quantity'];
                                        $total = $total + $cartItem['price']*$cartItem['quantity'];
                                        if($total_quantity >= $special_offer->min_qty){

                                            if($special_offer->discount_type == 'amount') {
                                                $discount = $special_offer->discount;
                                                $total -= $discount;

                                            }else if ($special_offer->discount_type == 'percent') {
                                                $discount = ($total * $special_offer->discount) / 100;
                                                if ($discount > $special_offer->max_discount) {
                                                    $discount = $special_offer->max_discount;
                                                }
                                                $total -= $discount;
                                            }
                                        }
                                    }
                                }
                           @endphp
                        @endif
                    @elseif($special_offer->offer_type == 'product_base')
                    @php
                        if($special_offer->discount_method == 'price_break_discount'){
                            $total = 0;
                            $offered_product_id = json_decode($special_offer->product_ids);
                            $offered_product_qty = json_decode($special_offer->min_qty_Price_break);
                            $offered_product_price = json_decode($special_offer->discount_Price_break);

                            $quantity_count = count($offered_product_price);
                            $discount = 0;
                            $offer_qty_exist = 0;

                            foreach ($carts as $key => $cartItem){
                                $item_quantity = $cartItem['quantity'];
                                $product_id = $cartItem['product_id'];
                                $total = $total + $cartItem['price']*$cartItem['quantity'];

                                if(in_array($product_id,$offered_product_id)){
                                    $offer_qty_exist += $cartItem['quantity'];
                                }
                            }
                            foreach($offered_product_qty as $key=>$offered_qty){
                                if($offered_qty == $offer_qty_exist){
                                    $discount = $offered_product_price[$key];
                                }else if($offer_qty_exist > $offered_product_qty[$key]){
                                    $discount = 0;
                                    $discount = $offered_product_price[$key];
                                }
                            }

                            $total -= $discount;
                        }else if($special_offer->discount_method == 'flat_discount_offer'){
                            $total = 0;
                            $offer_amount = 0;$discount = 0;
                            $offered_product_id = json_decode($special_offer->product_ids);

                            foreach ($carts as $key => $cartItem){
                                $item_quantity = $cartItem['quantity'];
                                $product_id = $cartItem['product_id'];
                                $total = $total + $cartItem['price']*$cartItem['quantity'];

                                if(in_array($product_id,$offered_product_id)){
                                    $offer_amount += $cartItem['price']*$cartItem['quantity'];
                                }
                            }

                            if($offer_amount >= $special_offer->min_buy && $date >= $special_offer->start_date && $date <= $special_offer->end_date){
                                if($special_offer->discount_type == 'amount') {
                                    $total -= $special_offer->discount;
                                }else if ($special_offer->discount_type == 'percent') {
                                    $discount = ($offer_amount * $special_offer->discount) / 100;
                                    if ($discount > $special_offer->max_discount) {
                                        $discount = $special_offer->max_discount;
                                    }
                                    $total -= $discount;
                                }
                            }
                        }else if($special_offer->discount_method == 'discount_on_quantiry'){
                            $total = 0;
                            $offer_amount = 0;$discount = 0;$offer_quantity=0;
                            $offered_product_id = json_decode($special_offer->product_ids);

                            foreach ($carts as $key => $cartItem){
                                $item_quantity = $cartItem['quantity'];
                                $product_id = $cartItem['product_id'];
                                $total = $total + $cartItem['price']*$cartItem['quantity'];

                                if(in_array($product_id,$offered_product_id)){
                                    $offer_amount += $cartItem['price']*$cartItem['quantity'];
                                    $offer_quantity += $cartItem['quantity'];
                                }
                            }
                            if($offer_quantity >= $special_offer->min_qty && $date >= $special_offer->start_date && $date <= $special_offer->end_date){
                                if($special_offer->discount_type == 'amount') {
                                    $total -= $special_offer->discount;
                                }else if ($special_offer->discount_type == 'percent') {
                                    $discount = ($offer_amount * $special_offer->discount) / 100;
                                    if ($discount > $special_offer->max_discount) {
                                        $discount = $special_offer->max_discount;
                                    }
                                    $total -= $discount;
                                }
                            }

                        }else if($special_offer->discount_method == 'quantity_offer'){
                            $total = 0;
                            $discount = 0;$offer_quantity=0;
                            $offered_product_id = json_decode($special_offer->product_ids);

                            foreach ($carts as $key => $cartItem){
                                $product_id = $cartItem['product_id'];
                                $total = $total + $cartItem['price']*$cartItem['quantity'];
                                if(in_array($product_id,$offered_product_id)){
                                    $offer_quantity += $cartItem['quantity'];
                                }
                            }
                            if($offer_quantity >= $special_offer->min_qty){
                                $discount = $cartItem['price']*$special_offer->discount_qty;
                            }
                            $total -= $discount;

                        }
                    @endphp
                    @endif
                @endif
                <tr class="cart-shipping">
                    <th>{{translate('Total Shipping')}}</th>
                    <td class="text-right">
                        <span class="font-italic">{{ single_price($shipping) }}</span>
                    </td>
                </tr>

                @if (Session::has('club_point'))
                    <tr class="cart-shipping">
                        <th>{{translate('Redeem point')}}</th>
                        <td class="text-right">
                            <span class="font-italic">{{ single_price(Session::get('club_point')) }}</span>
                        </td>
                    </tr>
                @endif

                @if ($carts[0]['discount'])
                    <tr class="cart-shipping">
                        <th>{{translate('Coupon Discount')}}</th>
                        <td class="text-right">
                            <span class="font-italic">{{ single_price($carts[0]['discount']) }}</span>
                        </td>
                    </tr>
                @endif

                @php
                    $total = $subtotal+$tax+$shipping;
                    if(Session::has('club_point')) {
                        $total -= Session::get('club_point');
                    }
                    if ($carts[0]['discount']){
                        $total -= $carts[0]['discount'];
                    }
                @endphp

                @if(isset($discount))
                    <tr class="cart-shipping">
                        <th>{{translate('Special Discount')}}</th>
                        <td class="text-right">
                            <span class="font-italic">{{ single_price($discount) }}</span>
                        </td>
                    </tr>
                    <tr class="cart-total">
                        <th><span class="strong-600">{{translate('Total')}}</span></th>
                        <td class="text-right">
                            <strong><span>{{ single_price($total-$discount) }}</span></strong>
                        </td>
                    </tr>
                @else
                    <tr class="cart-total">
                        <th><span class="strong-600">{{translate('Total')}}</span></th>
                        <td class="text-right">
                            <strong><span>{{ single_price($total) }}</span></strong>
                        </td>
                    </tr>
                @endif



            </tfoot>
        </table>

        @if (\App\Addon::where('unique_identifier', 'club_point')->first() != null && \App\Addon::where('unique_identifier', 'club_point')->first()->activated)
            @if (Session::has('club_point'))
                <div class="mt-3">
                    <form class="" action="{{ route('checkout.remove_club_point') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="input-group">
                            <div class="form-control">{{ Session::get('club_point')}}</div>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-primary">{{translate('Remove Redeem Point')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            @else
                {{--@if(Auth::user()->point_balance > 0)
                    <div class="mt-3">
                        <p>
                            {{translate('Your club point is')}}:
                            @if(isset(Auth::user()->point_balance))
                                {{ Auth::user()->point_balance }}
                            @endif
                        </p>
                        <form class="" action="{{ route('checkout.apply_club_point') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="input-group">
                                <input type="text" class="form-control" name="point" placeholder="{{translate('Enter club point here')}}" required>
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-primary">{{translate('Redeem')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                @endif--}}
            @endif
        @endif

        @if (Auth::check() && get_setting('coupon_system') == 1)
            @if ($carts[0]['discount'] > 0)
                <div class="mt-3">
                    <form class="" id="remove-coupon-form" action="{{ route('checkout.remove_coupon_code') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="owner_id" value="{{ $carts[0]['owner_id'] }}">
                        <div class="input-group">
                            <div class="form-control">{{ $carts[0]['coupon_code'] }}</div>
                            <div class="input-group-append">
                                <button type="button" id="coupon-remove" class="btn btn-primary">{{translate('Change Coupon')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            @else
                <div class="mt-3">
                    <form class="" id="apply-coupon-form" action="{{ route('checkout.apply_coupon_code') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="owner_id" value="{{ $carts[0]['owner_id'] }}">
                        <div class="input-group">
                            <input type="text" class="form-control" name="code" placeholder="{{translate('Have coupon code? Enter here')}}" required>
                            <div class="input-group-append">
                                <button type="button" id="coupon-apply" class="btn btn-primary">{{translate('Apply')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            @endif
        @endif

    </div>
</div>
