@php
if(auth()->user() != null) {
    $user_id = Auth::user()->id;
    $cart = \App\Cart::where('user_id', $user_id)->get();
} else {
    $temp_user_id = Session()->get('temp_user_id');
    if($temp_user_id) {
        $cart = \App\Cart::where('temp_user_id', $temp_user_id)->get();
    }
}

@endphp
<a href="javascript:void(0)" class="d-flex align-items-center text-reset h-100" data-toggle="dropdown" data-display="static">
    <i class="la la-shopping-cart la-3x opacity-80"></i>
    <span class="flex-grow-1 ml-1">
        @if(isset($cart) && count($cart) > 0)
            <span class="badge badge-primary badge-inline badge-pill">
                {{ count($cart)}}
            </span>
        @else
            <span class="badge badge-primary badge-inline badge-pill">0</span>
        @endif
        <span class="nav-box-text d-none d-xl-block opacity-70">{{translate('My Cart')}}</span>
    </span>
</a>
<div class="dropdown-menu dropdown-menu-right dropdown-menu-lg p-0 stop-propagation">

    @if(isset($cart) && count($cart) > 0)
        <div class="p-3 fs-15 fw-600 p-3 border-bottom">
            {{translate('Cart Items')}}
        </div>
        <ul class="h-250px overflow-auto c-scrollbar-light list-group list-group-flush">
            @php
                $total = 0;
            @endphp
            @foreach($cart as $key => $cartItem)
                @php
                    $product = \App\Product::find($cartItem['product_id']);
                    $total = $total + $cartItem['price'] * $cartItem['quantity'];
                    if ($cartItem['variation'] != null) {
                        $product_name_with_choice = $product->getTranslation('name').' - '.$cartItem['variation'];
                    }else{
                        $product_name_with_choice = $product->getTranslation('name');
                    }
                @endphp
                @if($cartItem['variation'] != null)
                    @foreach($product->stocks as $stock)
                        @if($stock->variant == $cartItem['variation'])
                            @php
                                $image = $stock['image'];
                            @endphp
                        @endif
                    @endforeach
                @else
                    @php
                        $image = $product->thumbnail_img;
                    @endphp
                @endif
                @if ($product != null)
                    <li class="list-group-item">
                        <span class="d-flex align-items-center">
                            <a href="{{ route('product', $product->slug) }}" class="text-reset d-flex align-items-center flex-grow-1">
                                <img
                                    src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                    data-src="{{ uploaded_asset($image) }}"
                                    class="img-fit lazyload size-60px rounded"
                                    alt="{{  $product->getTranslation('name')  }}"
                                >
                                <span class="minw-0 pl-2 flex-grow-1">
                                    <span class="fw-600 mb-1 text-truncate-2">
                                       {{ $product_name_with_choice}}
                                    </span>
                                    <span class="">{{ $cartItem['quantity'] }}x</span>
                                    <span class="">{{ single_price($cartItem['price']) }}</span>
                                </span>
                            </a>
                            <span class="">
                                <button onclick="removeFromCart({{ $cartItem['id'] }})" class="btn btn-sm btn-icon stop-propagation">
                                    <i class="la la-close"></i>
                                </button>
                            </span>
                        </span>
                    </li>
                @endif
            @endforeach

            @php
                $special_offer = \App\SpecialOffer::where('status', 1)->latest()->first();
                $date = strtotime(date('d-m-Y'));
            @endphp

            @if(isset($special_offer) && $special_offer != null && $date >= $special_offer->start_date && $date <= $special_offer->end_date)
                @if ($special_offer->offer_type == 'product_base')
                @if ($special_offer->discount_method == 'buy_get_offer' && $date >= $special_offer->start_date && $date <= $special_offer->end_date)
                    @foreach ($cart as $key => $cartItem)
                        @php
                            $product_id_belong_offer = json_decode($special_offer->product_ids);
                            $product_id_with_offer = $special_offer->offer_product_ids;
                            $product_id = $cartItem['product_id'];

                        @endphp
                        @if(in_array($product_id,$product_id_belong_offer))
                            @php
                                $product = \App\Product::find($product_id_with_offer);
                                $image_thum = $product->thumbnail_img;
                                $cartItem['price'] = 0;
                                $cartItem['tax'] = 0;
                            @endphp
                            <li class="list-group-item">
                                <span class="d-flex align-items-center">
                                    <a href="{{ route('product', $product->slug) }}" class="text-reset d-flex align-items-center flex-grow-1">
                                        <img
                                            src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                            data-src="{{ uploaded_asset($image_thum) }}"
                                            class="img-fit lazyload size-60px rounded"
                                            alt="{{  $product->getTranslation('name')  }}"
                                        >
                                        <span class="minw-0 pl-2 flex-grow-1">
                                            <span class="fw-600 mb-1 text-truncate-2">
                                               {{ $product->name}}
                                            </span>
                                            <span class="">{{ $cartItem['quantity'] }}x</span>
                                            <span class="">{{ single_price($cartItem['price']) }}</span>
                                        </span>
                                    </a>
                                </span>
                            </li>
                        @endif
                    @endforeach
                @endif
            @endif
            @endif
        </ul>
        @php
            $special_offer = \App\SpecialOffer::where('status', 1)->latest()->first();
            $date = strtotime(date('d-m-Y'));
            $total_quantity = 0;
            $free_discount = 0;

            if (isset($special_offer) && $special_offer != null && $date >= $special_offer->start_date && $date <= $special_offer->end_date) {
                if($special_offer->offer_type == 'cart_base'){
                    if($special_offer->discount_method == 'flat_discount_offer'){
                        if($special_offer != null){
                            if($total >= $special_offer->min_buy && $date >= $special_offer->start_date && $date <= $special_offer->end_date){
                                if($special_offer->discount_type == 'amount') {
                                    $total -= $special_offer->discount;
                                }else if ($special_offer->discount_type == 'percent') {
                                    $discount = ($total * $special_offer->discount) / 100;
                                    if ($discount > $special_offer->max_discount) {
                                        $discount = $special_offer->max_discount;
                                    }
                                    $total -= $discount;
                                }
                            }
                        }
                    }else if($special_offer->discount_method == 'quantity_offer'){
                        if($date >= $special_offer->start_date && $date <= $special_offer->end_date){
                            foreach ($cart as $key => $cartItem){
                                $total_quantity += $cartItem['quantity'];
                                if($total_quantity >= $special_offer->min_qty){
                                    $free_discount = $cartItem['price']*$special_offer->discount_qty;
                                }
                            }
                            $total -= $free_discount;
                        }
                    }if($special_offer->discount_method == 'discount_on_quantiry'){
                        $total = 0;
                        if($date >= $special_offer->start_date && $date <= $special_offer->end_date){
                            foreach ($cart as $key => $cartItem){
                                $total_quantity += $cartItem['quantity'];
                                $total = $total + $cartItem['price']*$cartItem['quantity'];
                                if($total_quantity >= $special_offer->min_qty){

                                    if($special_offer->discount_type == 'amount') {
                                    $total -= $special_offer->discount;

                                    }else if ($special_offer->discount_type == 'percent') {
                                        $discount = ($total * $special_offer->discount) / 100;
                                        if ($discount > $special_offer->max_discount) {
                                            $discount = $special_offer->max_discount;
                                        }
                                        $total -= $discount;
                                    }

                                }
                            }
                        }
                    }
            }else if($special_offer->offer_type == 'product_base'){
                if($special_offer->discount_method == 'price_break_discount'){
                $total = 0;
                $offered_product_id = json_decode($special_offer->product_ids);
                $offered_product_qty = json_decode($special_offer->min_qty_Price_break);
                $offered_product_price = json_decode($special_offer->discount_Price_break);

                $quantity_count = count($offered_product_price);
                $discount = 0;
                $offer_qty_exist = 0;$discount_offer = 0;

                foreach ($cart as $key => $cartItem){
                    $item_quantity = $cartItem['quantity'];
                    $product_id = $cartItem['product_id'];
                    $total = $total + $cartItem['price']*$cartItem['quantity'];

                    if(in_array($product_id,$offered_product_id)){
                        $offer_qty_exist += $cartItem['quantity'];
                    }
                }
                foreach($offered_product_qty as $key=>$offered_qty){
                    if($offered_qty == $offer_qty_exist){
                        $discount_offer = $offered_product_price[$key];
                    }else if($offer_qty_exist > $offered_product_qty[$key]){
                        $discount_offer = 0;
                        $discount_offer = $offered_product_price[$key];
                    }
                }

                $total -= $discount_offer;
                }else if($special_offer->discount_method == 'flat_discount_offer'){
                    $total = 0;
                    $offer_amount = 0;$discount = 0;
                    $offered_product_id = json_decode($special_offer->product_ids);

                    foreach ($cart as $key => $cartItem){
                        $item_quantity = $cartItem['quantity'];
                        $product_id = $cartItem['product_id'];
                        $total = $total + $cartItem['price']*$cartItem['quantity'];

                        if(in_array($product_id,$offered_product_id)){
                            $offer_amount += $cartItem['price']*$cartItem['quantity'];
                        }
                    }

                    if($offer_amount >= $special_offer->min_buy && $date >= $special_offer->start_date && $date <= $special_offer->end_date){
                        if($special_offer->discount_type == 'amount') {
                            $total -= $special_offer->discount;
                        }else if ($special_offer->discount_type == 'percent') {
                            $discount = ($offer_amount * $special_offer->discount) / 100;
                            if ($discount > $special_offer->max_discount) {
                                $discount = $special_offer->max_discount;
                            }
                            $total -= $discount;
                        }
                    }
                }else if($special_offer->discount_method == 'discount_on_quantiry'){
                    $total = 0;
                    $offer_amount = 0;$discount = 0;$offer_quantity=0;
                    $offered_product_id = json_decode($special_offer->product_ids);

                    foreach ($cart as $key => $cartItem){
                        $item_quantity = $cartItem['quantity'];
                        $product_id = $cartItem['product_id'];
                        $total = $total + $cartItem['price']*$cartItem['quantity'];

                        if(in_array($product_id,$offered_product_id)){
                            $offer_amount += $cartItem['price']*$cartItem['quantity'];
                            $offer_quantity += $cartItem['quantity'];
                        }
                    }
                    if($offer_quantity >= $special_offer->min_qty && $date >= $special_offer->start_date && $date <= $special_offer->end_date){
                        if($special_offer->discount_type == 'amount') {
                            $total -= $special_offer->discount;
                        }else if ($special_offer->discount_type == 'percent') {
                            $discount = ($offer_amount * $special_offer->discount) / 100;
                            if ($discount > $special_offer->max_discount) {
                                $discount = $special_offer->max_discount;
                            }
                            $total -= $discount;
                        }
                    }

                }else if($special_offer->discount_method == 'quantity_offer'){
                    $total = 0;
                    $discount = 0;$offer_quantity=0;
                    $offered_product_id = json_decode($special_offer->product_ids);

                    foreach ($cart as $key => $cartItem){
                        $product_id = $cartItem['product_id'];
                        $total = $total + $cartItem['price']*$cartItem['quantity'];
                        if(in_array($product_id,$offered_product_id)){
                            $offer_quantity += $cartItem['quantity'];
                        }
                    }
                    if($offer_quantity >= $special_offer->min_qty){
                        $discount = $cartItem['price']*$special_offer->discount_qty;
                    }
                    $total -= $discount;

                }
            }
            }

        @endphp
        <div class="px-3 py-2 fs-15 border-top d-flex justify-content-between">
            <span class="opacity-60">{{translate('Subtotal')}}</span>
            <span class="fw-600">{{ single_price($total) }}</span>
        </div>
        <div class="px-3 py-2 text-center border-top">
            <ul class="list-inline mb-0">
                <li class="list-inline-item">
                    <a href="{{ route('cart') }}" class="btn btn-soft-primary btn-sm">
                        {{translate('View cart')}}
                    </a>
                </li>
                @if (Auth::check())
                <li class="list-inline-item">
                    <a href="{{ route('checkout.shipping_info') }}" class="btn btn-primary btn-sm">
                        {{translate('Checkout')}}
                    </a>
                </li>
                @endif
            </ul>
        </div>
    @else
        <div class="text-center p-3">
            <i class="las la-frown la-3x opacity-60 mb-3"></i>
            <h3 class="h6 fw-700">{{translate('Your Cart is empty')}}</h3>
        </div>
    @endif

</div>
