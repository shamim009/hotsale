<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SizeDiscription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('size_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->string('size');
            $table->string('chest')->nullable();
            $table->string('length')->nullable();
            $table->string('shoulder')->nullable();
            $table->string('collar')->nullable();
            $table->string('sleeve')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('size_details');
    }
}
